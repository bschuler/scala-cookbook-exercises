package typedTest

import AbstractKenny.Command
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{
  ActorRef,
  Behavior,
  PostStop,
  PreRestart,
  Signal,
  SupervisorStrategy
}

object AbstractKenny:
  sealed trait Command
  case object ForceRestart                     extends Command
  final case class SendString(message: String) extends Command
  case object Seppuku                          extends Command

  def apply(): Behavior[Command] = Behaviors
    .supervise(
      Behaviors.setup[Command](context => new AbstractKenny(context))
    )
    .onFailure(SupervisorStrategy.restart)

class AbstractKenny(context: ActorContext[Command])
    extends AbstractBehavior[Command](context):
  import AbstractKenny.*

  context.log.info("entered the AbstractKenny constructor")

  override def onMessage(msg: Command): Behavior[Command] =
    msg match
      case ForceRestart =>
        throw new Exception("Boom!")
      case SendString(s) =>
        context.log.info(s"AbstractKenny received string $s")
        this
      case Seppuku =>
        context.log.info("AbstractKenny ends its days")
        Behaviors.stopped

  override def onSignal: PartialFunction[Signal, Behavior[Command]] = {
    case PreRestart =>
      context.log.info("AbstractKenny: preRestart")
      this
    case PostStop =>
      context.log.info("AbstractKenny: postStop")
      this
  }
