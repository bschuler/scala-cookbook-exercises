package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.{ActorSystem, Behavior, PostStop, Signal}

case object Pigeon:
  sealed trait Command

  case object Pioupiou extends Command
  case object Stop     extends Command

  def apply(): Behavior[Pigeon.Command] =
    Behaviors.setup(context => new Pigeon(context).init)

case class Pigeon(context: ActorContext[Pigeon.Command]):
  def init: Behavior[Pigeon.Command] = Behaviors.receiveMessagePartial {
    case Pigeon.Pioupiou =>
      context.log.info("pioupiou")
      Behaviors.same
    case Pigeon.Stop =>
      context.log.info("stopping")
      Behaviors.stopped
  }
object PigeonManager:
  sealed trait Command
  case class Create(name: String) extends Command

  case object Hello                            extends Command
  case class PiouPiou(name: String)            extends Command
  case class CreateTmp(name: String)           extends Command
  case class CreateTmpOutsideMap(name: String) extends Command
  case class Stop(name: String)                extends Command
  case class CreateWatched(name: String)       extends Command
  case class PrintActorName(name: String)      extends Command

  def apply(): Behavior[PigeonManager.Command] =
    Behaviors.setup(context => new PigeonManager(context).init(Map.empty))

class PigeonManager(context: ActorContext[PigeonManager.Command]):

  def init(
    pigeons: Map[String, ActorRef[Pigeon.Command]]
  ): Behavior[PigeonManager.Command] = Behaviors.receiveMessagePartial {
    case PigeonManager.Create(name: String) =>
      if pigeons.contains(name) then
        val pigeonRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Pigeon(), name = name)
        init(pigeons - name + (name -> pigeonRef))
      else
        val pigeonRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Pigeon(), name = name)
        init(pigeons + (name -> pigeonRef))
    case PigeonManager.CreateWatched(name: String) =>
      val pigeonRef: ActorRef[Pigeon.Command] =
        context.spawn(behavior = Pigeon(), name = name)
      context.watchWith(pigeonRef, PigeonManager.CreateWatched(name))
      init(pigeons + (name -> pigeonRef))
    case PigeonManager.Hello =>
      context.log.info("hello")
      Behaviors.same
    case PigeonManager.PiouPiou(name) =>
      if pigeons.contains(name) then
        pigeons(name) ! Pigeon.Pioupiou
        Behaviors.same
      else
        context.log.info(s"cannot pioupiou to $name, there are no such pigeon")
        Behaviors.same
    case PigeonManager.CreateTmp(name) =>
      if pigeons.contains(name) then
        pigeons(name) ! Pigeon.Stop
        val tmpRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Behaviors.stopped, name = s"tmp-$name")
        init(pigeons - name + (name -> tmpRef))
      else
        val tmpRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Behaviors.stopped, name = s"tmp-$name")
        init(pigeons + (name -> tmpRef))
    case PigeonManager.Stop(name) =>
      if pigeons.contains(name) then
        pigeons(name) ! Pigeon.Stop
        init(pigeons - name)
      else Behaviors.same
    case PigeonManager.CreateTmpOutsideMap(name) =>
      val tmpRef: ActorRef[Pigeon.Command] =
        context.spawn(behavior = Behaviors.stopped, name = s"tmp-$name")
      Behaviors.same
    case PigeonManager.PrintActorName(name) =>
      if pigeons.contains(name) then
        context.log.info(s"the name is ${pigeons(name).path.name}")
        Behaviors.same
      else
        context.log.info(s"no such member with $name")
        Behaviors.same
  }

object TmpActorsViability extends App:
  val actor1 = ActorSystem(PigeonManager(), "pigeonManager")
  actor1 ! PigeonManager.Hello
//  actor1 ! PigeonManager.Create("gary")
//  actor1 ! PigeonManager.PiouPiou("gary")
//  actor1 ! PigeonManager.Stop("gary")
//  Thread.sleep(1000)
//  actor1 ! PigeonManager.Create("gary")
//  actor1 ! PigeonManager.PiouPiou("gary")

  actor1 ! PigeonManager.CreateTmp("bob")
  actor1 ! PigeonManager.PiouPiou("bob")
  actor1 ! PigeonManager.PrintActorName("bob")
  Thread.sleep(1000)
  actor1 ! PigeonManager.PrintActorName("bob")
  actor1 ! PigeonManager.CreateTmp("bob")
  actor1 ! PigeonManager.PiouPiou("bob")
  actor1 ! PigeonManager.PrintActorName("bob")

//  actor1 ! PigeonManager.CreateWatched("joe")
//  actor1 ! PigeonManager.PiouPiou("joe")
//  actor1 ! PigeonManager.Stop("joe")
//  actor1 ! PigeonManager.PiouPiou("joe")
//  actor1 ! PigeonManager.Stop("joe")
//  actor1 ! PigeonManager.PiouPiou("joe")
//  Thread.sleep(1000)
//  actor1 ! PigeonManager.PiouPiou("joe")
//  actor1 ! PigeonManager.PiouPiou("joe")

//  actor1 ! PigeonManager.CreateTmpOutsideMap("jack")
//  actor1 ! PigeonManager.PiouPiou("jack")
//  Thread.sleep(1000)
//  actor1 ! PigeonManager.CreateTmpOutsideMap("jack")
//  actor1 ! PigeonManager.PiouPiou("jack")
//  Thread.sleep(1000)
//  actor1 ! PigeonManager.CreateTmpOutsideMap("jack")
//  actor1 ! PigeonManager.PiouPiou("jack")
//  Thread.sleep(1000)
//  actor1 ! PigeonManager.CreateTmpOutsideMap("jack")
//  actor1 ! PigeonManager.PiouPiou("jack")
