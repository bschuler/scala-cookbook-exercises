package typedTest

import TypedKenny.Command
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{
  ActorRef,
  Behavior,
  PostStop,
  PreRestart,
  Signal,
  SupervisorStrategy
}
import org.apache.pekko.actor.typed.scaladsl.Behaviors.{
  receive,
  receiveMessagePartial
}

object TypedKenny:
  sealed trait Command
  case object ForceRestart                     extends Command
  final case class SendString(message: String) extends Command
  case object Seppuku                          extends Command

  def apply(): Behavior[Command] = Behaviors
    .supervise(
      Behaviors.setup[Command](context => new TypedKenny(context).init)
    )
    .onFailure(SupervisorStrategy.restart)

class TypedKenny(context: ActorContext[Command]):
  import TypedKenny.*

  context.log.info("entered the TypedKenny constructor")

  val init: Behavior[Command] = receiveMessagePartial[Command] {
    case ForceRestart =>
      throw new Exception("Boom!")
    case SendString(s) =>
      context.log.info(s"TypedKenny received string $s")
      Behaviors.same
    case Seppuku =>
      context.log.info("TypedKenny ends its days")
      Behaviors.stopped
  }.receiveSignal {
    case (_, PreRestart) =>
      context.log.info("gonna restart")
      Behaviors.same
    case (_, PostStop) =>
      context.log.info("TypedKenny: postStop")
      Behaviors.same
  }
