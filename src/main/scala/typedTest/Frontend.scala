package typedTest

import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.actor.typed.{ActorRef, Behavior}

import java.net.URI

object Backend:
  sealed trait Request
  final case class StartTranslationJob(
    taskId: Int,
    site: URI,
    replyTo: ActorRef[Response]
  ) extends Request

  sealed trait Response
  final case class JobStarted(taskId: Int)                    extends Response
  final case class JobProgress(taskId: Int, progress: Double) extends Response
  final case class JobCompleted(taskId: Int, result: URI)     extends Response

object Frontend:

  sealed trait Command
  final case class Translate(site: URI, replyTo: ActorRef[URI]) extends Command

  private type CommandAndResponse = Command | Backend.Response // (1)

  def apply(backend: ActorRef[Backend.Request]): Behavior[Command] = // (2)
    Behaviors
      .setup[CommandAndResponse] { context =>

        def active(
          inProgress: Map[Int, ActorRef[URI]],
          count: Int
        ): Behavior[CommandAndResponse] =
          Behaviors.receiveMessage[CommandAndResponse] {
            case Translate(site, replyTo) =>
              val taskId = count + 1
              backend ! Backend.StartTranslationJob(
                taskId,
                site,
                context.self
              ) // (3)
              active(inProgress.updated(taskId, replyTo), taskId)

            case Backend.JobStarted(taskId) => // (4)
              context.log.info("Started {}", taskId)
              Behaviors.same
            case Backend.JobProgress(taskId, progress) =>
              context.log.info(s"Progress $taskId: $progress")
              Behaviors.same
            case Backend.JobCompleted(taskId, result) =>
              context.log.info(s"Completed $taskId: $result")
              inProgress(taskId) ! result
              active(inProgress - taskId, count)
          }

        active(inProgress = Map.empty, count = 0)
      }
      .narrow
