package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.{ActorSystem, Behavior}

case class PigeonCage(actor: ActorRef[Pigeon.Command], anything: String)
def pigeonCage(
  actor: ActorRef[Pigeon.Command],
  anything: String = "anything"
): PigeonCage = PigeonCage(actor, anything)
object PigeonManagerEncapsulated:
  sealed trait Command
  case class Create(name: String) extends Command

  case object Hello                       extends Command
  case class PiouPiou(name: String)       extends Command
  case class CreateTmp(name: String)      extends Command
  case class PrintActorName(name: String) extends Command

  def apply(): Behavior[PigeonManagerEncapsulated.Command] =
    Behaviors.setup(context =>
      new PigeonManagerEncapsulated(context).init(Map.empty)
    )

class PigeonManagerEncapsulated(
  context: ActorContext[PigeonManagerEncapsulated.Command]
):

  def init(
    Cage: Map[String, PigeonCage]
  ): Behavior[PigeonManagerEncapsulated.Command] =
    Behaviors.receiveMessagePartial {
      case PigeonManagerEncapsulated.Create(name: String) =>
        val pigeonRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Pigeon(), name = name)
        val cg = pigeonCage(pigeonRef)
        if Cage.contains(name) then init(Cage - name + (name -> cg))
        else init(Cage + (name                               -> cg))
      case PigeonManagerEncapsulated.Hello =>
        context.log.info("hello")
        Behaviors.same
      case PigeonManagerEncapsulated.PiouPiou(name) =>
        if Cage.contains(name) then
          Cage(name).actor ! Pigeon.Pioupiou
          Behaviors.same
        else
          context.log.info(
            s"cannot pioupiou to $name, there are no such pigeon"
          )
          Behaviors.same
      case PigeonManagerEncapsulated.CreateTmp(name) =>
        val tmpRef: ActorRef[Pigeon.Command] =
          context.spawn(behavior = Behaviors.stopped, name = s"tmp-$name")
        val cg = pigeonCage(tmpRef)
        init(Cage + (name -> cg))
      case PigeonManagerEncapsulated.PrintActorName(name) =>
        if Cage.contains(name) then
          context.log.info(s"the name is ${Cage(name).actor.path.name}")
          Behaviors.same
        else
          context.log.info(s"no such member with $name")
          Behaviors.same
    }

object TmpEncapsulated extends App:
  val actor1 = ActorSystem(PigeonManagerEncapsulated(), "pigeonManager")
  actor1 ! PigeonManagerEncapsulated.Hello

//  actor1 ! PigeonManagerEncapsulated.Create("bob")
//  actor1 ! PigeonManagerEncapsulated.PiouPiou("bob")
//  actor1 ! PigeonManagerEncapsulated.PrintActorName("bob")
//  Thread.sleep(1000)
//  actor1 ! PigeonManagerEncapsulated.PrintActorName("bob")
//  Thread.sleep(1000)
//  actor1 ! PigeonManagerEncapsulated.PrintActorName("bob")
//  Thread.sleep(1000)
//  actor1 ! PigeonManagerEncapsulated.PrintActorName("bob")

  actor1 ! PigeonManagerEncapsulated.CreateTmp("joe")
  actor1 ! PigeonManagerEncapsulated.PiouPiou("joe")
  actor1 ! PigeonManagerEncapsulated.PrintActorName("joe")
  Thread.sleep(1000)
  actor1 ! PigeonManagerEncapsulated.PrintActorName("joe")
  Thread.sleep(1000)
  actor1 ! PigeonManagerEncapsulated.PrintActorName("joe")
  Thread.sleep(1000)
  actor1 ! PigeonManagerEncapsulated.PrintActorName("joe")
