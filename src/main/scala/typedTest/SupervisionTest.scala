package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.*
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
object SupervisingActor:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new SupervisingActor(context))

class SupervisingActor(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  private val child = context.spawn(
    Behaviors
      .supervise(SupervisedActor())
      .onFailure(SupervisorStrategy.restart),
    name = "supervised-actor"
  )

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "failChild" =>
        child ! "fail"
        this
      case _ =>
        println("hello from supervisor")
        this

object SupervisedActor:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new SupervisedActor(context))

class SupervisedActor(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  println("supervised actor started")

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "fail" =>
        println("supervised actor fails now")
        throw new Exception("I failed!")
      case "failChild" =>
        println("talk to my manager")
        this
      case _ =>
        println("ayoo")
        this

  override def onSignal: PartialFunction[Signal, Behavior[String]] =
    case PreRestart =>
      println("supervised actor will be restarted")
      this
    case PostStop =>
      println("supervised actor stopped")
      this

object SupervisionTest extends App:
//    val actorSystem = ActorSystem(Behaviors.empty[String],"main")
//    val actor1 = actorSystem.spawn(SupervisedActor(), "ayo")
  val actor1: ActorSystem[String] =
    ActorSystem(SupervisingActor(), "supervising-actor")
  // actor1 ! "hi"
  actor1 ! "failChild"
  Thread.sleep(3000)
