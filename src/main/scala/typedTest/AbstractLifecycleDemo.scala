package typedTest

import actortests.ForceRestart
import org.apache.pekko.actor.PoisonPill
import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  Props,
  SpawnProtocol
}
import org.apache.pekko.util.Timeout

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.actor.typed.scaladsl.AskPattern.Askable
import org.apache.pekko.actor.typed.scaladsl.AskPattern.schedulerFromActorSystem

import scala.util.Success

object AbstractLifecycleDemo extends App:

  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "Guardian")

  given ActorSystem[SpawnProtocol.Command] = system

  val ec: ExecutionContext = system.executionContext

  given ExecutionContext = ec

  given Timeout = Timeout(5.seconds)

  val fKenny: Future[ActorRef[AbstractKenny.Command]] = system.ask(
    SpawnProtocol.Spawn(
      behavior = AbstractKenny.apply(),
      name = "AbstractKenny",
      props = Props.empty,
      _
    )
  )
  fKenny.onComplete {
    case Success(kenny) =>
      println("sending kenny a simple String message")
      kenny ! AbstractKenny.SendString("hello")
      Thread.sleep(2000)

      println("make kenny restart")
      kenny ! AbstractKenny.ForceRestart
      Thread.sleep(2000)

      println("stopping kenny")
      kenny ! AbstractKenny.Seppuku
      Thread.sleep(2000)
    case _ => println("fuck my life")
  }
