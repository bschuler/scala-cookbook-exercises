package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem, Behavior}

object PrintMyActorRefActor:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new PrintMyActorRefActor(context))

class PrintMyActorRefActor(context: ActorContext[String])
    extends AbstractBehavior[String](context):

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "printit" =>
        val secondRef = context.spawn(Behaviors.empty[String], "second-actor")
        println(s"Second: $secondRef")
        this
      case "hello" =>
        context.log.info("parent says hello")
        this
      case "createSecondSon" =>
        val secondSon = context.spawn(SecondSon(context.self), "second-son")
        secondSon ! SecondSon.Hello
        secondSon ! SecondSon.CreateThird
        this

object SecondSon:
  sealed trait Command
  object Hello       extends Command
  object CreateThird extends Command
  def apply(parent: ActorRef[String]): Behavior[Command] =
    Behaviors.setup(context => new SecondSon(context, parent).initState)

class SecondSon(
  context: ActorContext[SecondSon.Command],
  parent: ActorRef[String]
):
  import SecondSon.*
  import ThirdSon.Ahoy

  val initState: Behavior[Command] = Behaviors.receiveMessagePartial {
    case Hello =>
      context.log.info("SecondSon says Hello")
      parent ! "hello"
      Behaviors.same
    case CreateThird =>
      context.log.info("creating third son")
      val thirdSon: ActorRef[ThirdSon.Command] =
        context.spawn(ThirdSon(context.self), "third-son")
      thirdSon ! ThirdSon.Ahoy
      Behaviors.same
  }

  object ThirdSon:
    sealed trait Command

    object Ahoy extends Command

    def apply(parent: ActorRef[SecondSon.Command]): Behavior[Command] =
      Behaviors.setup(context => new ThirdSon(context, parent).initState)

  class ThirdSon(
    context: ActorContext[ThirdSon.Command],
    parent: ActorRef[SecondSon.Command]
  ):

    import ThirdSon.*
    import SecondSon.*

    val initState: Behavior[ThirdSon.Command] =
      Behaviors.receiveMessagePartial { case Ahoy =>
        context.log.info("ThirdSon says Hello")
        parent ! SecondSon.Hello
        Behaviors.same
      }

object Main:
  def apply(): Behavior[String] = Behaviors.setup(context => new Main(context))

class Main(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  override def onMessage(msg: String): Behavior[String] = msg match
    case "start" =>
      val firstRef = context.spawn(PrintMyActorRefActor(), "first-son")
      // println(s"Firstref : $firstRef")
      // firstRef ! "printit"
      firstRef ! "createSecondSon"
      this

object ActorHierarchyExperiments extends App:
  val actor1 = ActorSystem(Main(), "ActorHierarchyExperiments")
  actor1 ! "start"
