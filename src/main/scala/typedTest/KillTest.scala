package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.{ActorSystem, Behavior, PostStop, Signal}

object GrievingParent:
  sealed trait Command
  case object Create extends Command

  case object Hello extends Command

  case object Error extends Command

  case object ChildError extends Command

  case object Seppuku extends Command

  case object ChildDoesNotRespond extends Command

  def apply(): Behavior[GrievingParent.Command] =
    Behaviors.setup(context => new GrievingParent(context).init(None))

class GrievingParent(context: ActorContext[GrievingParent.Command]):

  def init(
    child: Option[ActorRef[String]]
  ): Behavior[GrievingParent.Command] = Behaviors.receiveMessagePartial {
    case GrievingParent.Create =>
      val gary: ActorRef[String] =
        context.spawn(behavior = KilledChild(), name = "gary")
      context.watchWith(gary, GrievingParent.ChildError)
      init(Some(gary))
    case GrievingParent.Error =>
      child.get ! "error"
      init(None)
    case GrievingParent.Seppuku =>
      context.unwatch(child.get)
      context.watchWith(child.get, GrievingParent.ChildDoesNotRespond)
      child.get ! "seppuku"
      init(None)
    case GrievingParent.Hello =>
      child.get ! "hello"
      Behaviors.same
    case GrievingParent.ChildDoesNotRespond =>
      context.log.info("gary probably died")
      Behaviors.same
    case GrievingParent.ChildError =>
      context.log.info("gary had something")
      Behaviors.same
  }

object KilledChild:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new KilledChild(context).init)

class KilledChild(context: ActorContext[String]):
  def init: Behavior[String] = Behaviors.receiveMessagePartial(onMessage =
    case message =>
      if message == "seppuku" then
        context.log.info("gary dies")
        Behaviors.stopped
      else if message == "error" then
        val absurd = 1 / 0
        Behaviors.same
      else
        context.log.info(s"Received message ${message}")
        Behaviors.same
  )

object KillTest extends App:
  val actor1 = ActorSystem(GrievingParent(), "main")
  actor1 ! GrievingParent.Create
  actor1 ! GrievingParent.Hello
  actor1 ! GrievingParent.Error
  Thread.sleep(1000)
  actor1 ! GrievingParent.Create
  actor1 ! GrievingParent.Hello
  actor1 ! GrievingParent.Seppuku
  //actor1 ! GrievingParent.Hello
