package typedTest

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{ActorSystem, Behavior, PostStop, Signal}

object StartStopActor1:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new StartStopActor1(context))

class StartStopActor1(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  println("first started")
  context.spawn(StartStopActor2(), "second")

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "stop" => Behaviors.stopped

  override def onSignal: PartialFunction[Signal, Behavior[String]] = {
    case PostStop =>
      println("first stopped")
      this
  }

object StartStopActor2:
  def apply(): Behavior[String] =
    Behaviors.setup(new StartStopActor2(_))

class StartStopActor2(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  println("second started")

  override def onMessage(msg: String): Behavior[String] =
    // no messages handled by this actor
    Behaviors.unhandled

  override def onSignal: PartialFunction[Signal, Behavior[String]] = {
    case PostStop =>
      println("second stopped")
      this
  }

object StartStopTest extends App:
  val actor1 = ActorSystem(StartStopActor1(), "main")
  actor1 ! "stop"
