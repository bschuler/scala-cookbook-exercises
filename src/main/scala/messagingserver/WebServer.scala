package messagingserver

import org.apache.pekko.actor.typed.{ActorSystem, SpawnProtocol}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.http.scaladsl.server.Directives.*
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.http.scaladsl.model.ws.{Message, TextMessage}
import org.apache.pekko.stream.ActorMaterializer
import org.apache.pekko.stream.scaladsl.Flow

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.io.StdIn
import scala.util.{Failure, Success}

object WebServer extends App:

  val system = ActorSystem(SpawnProtocol(), "messaging-actorsystem")
  given ActorSystem[SpawnProtocol.Command] = system
  given ExecutionContext                   = system.executionContext

  // define a basic route ("/") that returns a welcoming message
  def helloRoute: Route = pathEndOrSingleSlash {
    complete("Welcome to messaging service")
  }

  def affirmRoute = path("affirm") {
    handleWebSocketMessages(
      Flow[Message].collect { case TextMessage.Strict(text) =>
        TextMessage("You said " + text)
      }
    )
  }
  def messageRoute =
    // pattern matching on the URL
    pathPrefix("message" / Segment) { trainerId =>
      // await on the webflow materialization pending session actor creation by the spawnSystem
      Await
        .ready(ChatSessionMap.findOrCreate(trainerId).webflow(), Duration.Inf)
        .value
        .get match
        case Success(value) => handleWebSocketMessages(value)
        case Failure(exception) =>
          println("got some exception")
          failWith(exception)
    }

  // bind the route using HTTP to the server address and port
  val binding =
    Http()
      .newServerAt("localhost", 8080)
      .bindFlow(helloRoute ~ affirmRoute)
  println("Server running...")

  // kill the server with input
  StdIn.readLine()
  binding.flatMap(_.unbind()).onComplete(_ => system.terminate())
  println("Server is shut down")
