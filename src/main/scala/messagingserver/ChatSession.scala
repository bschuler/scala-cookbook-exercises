package messagingserver

import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  DispatcherSelector,
  Props,
  SpawnProtocol
}
import org.apache.pekko.actor.typed.SpawnProtocol.Spawn
import org.apache.pekko.http.scaladsl.model.ws.{Message, TextMessage}
import org.apache.pekko.stream.{FlowShape, OverflowStrategy}
import org.apache.pekko.stream.scaladsl.*
import org.apache.pekko.stream.typed.scaladsl.{ActorSink, ActorSource}
import org.apache.pekko.util.Timeout
import CoreChatEvents.*
import WebSocketsEvents.*
import org.apache.pekko.http.scaladsl.client.RequestBuilding.WithTransformation

class ChatSession(userId: String)(using
  system: ActorSystem[SpawnProtocol.Command]
):
  println("Spawning new chat session")

  import org.apache.pekko.actor.typed.scaladsl.AskPattern.*
  import scala.concurrent.duration.*
  import ChatSession.*
  given Timeout          = Timeout(3.seconds)
  given ExecutionContext = system.executionContext

  // asks to spawn an actor outside of the system
  private[this] val sessionActor: Future[ActorRef[CoreChatEvent]] =
    system.ask[ActorRef[CoreChatEvent]] { ref =>
      Spawn[CoreChatEvent](
        // initial behavior has no websockets connection
        behavior = SessionActor.receive(None),
        name = s"$userId-chat-session",
        props = Props.empty,
        replyTo = ref
      )
    }

  // because we have access to an actor in the future, we also only have access to this Flow in the future
  def webflow(): Future[Flow[Message, Message, _]] = ???
//  def webflow(): Future[Flow[Message, Message, _]] = sessionActor.map {
//    session =>
//      Flow.fromGraph(
//        // passing parameters allows us to instantiate them as stream resource inside the stream
//        GraphDSL.createGraph(webSocketsActor) { builder => socket =>
//
//          import GraphDSL.Implicits.*
//
//          // transforms messages from the websockets into the actor's protocol
//          val webSocketSource = builder.add(
//            Flow[Message].collect { case TextMessage.Strict(txt) =>
//              UserMessage(txt, "111-111-1111")
//            }
//          )
//
//          // transform a message from the WebSocketProtocol back into a websocket text message
//          val webSocketSink = builder.add(
//            Flow[WebSocketsEvent].collect { case MessageToUser(p, t) =>
//              TextMessage(p + t)
//            }
//          )
//
//          // route messages to the session actor
//          val routeToSession = builder.add(
//            ActorSink.actorRef[CoreChatEvent](
//              ref = session,
//              onCompleteMessage = Disconnected,
//              onFailureMessage = Failed.apply
//            )
//          )
//
//          // materialize the Source we supplied in the argument
//          val materializedActorSource =
//            builder.materializedValue
//              //.map(ref => Connected(ref))
//
//          // fan-in - combine two sources into one
//          val merge = builder.add(Merge[CoreChatEvent](2))
//          // ~> connects everything
//          webSocketSource ~> merge.in(0)
//          materializedActorSource ~> merge.in(1)
//          merge ~> routeToSession
//          socket ~> webSocketSink
//          // expose inlets/outlets
//          FlowShape(webSocketSource.in, webSocketSink.out)
//        }
//      )
//  }

object ChatSession:
  def apply(userId: String)(using
    system: ActorSystem[SpawnProtocol.Command]
  ): ChatSession = new ChatSession(userId)

  val webSocketsActor = ActorSource.actorRef[WebSocketsEvent](
    completionMatcher = { case Complete =>
    },
    failureMatcher = { case WebSocketsEvents.Failure(ex) =>
      throw ex
    },
    bufferSize = 5,
    OverflowStrategy.fail
  )
