package iot

import org.apache.pekko
import pekko.actor.typed.Behavior
import pekko.actor.typed.PostStop
import pekko.actor.typed.Signal
import pekko.actor.typed.scaladsl.AbstractBehavior
import pekko.actor.typed.scaladsl.ActorContext
import pekko.actor.typed.scaladsl.Behaviors

object IotSupervisor:
  def apply(): Behavior[Nothing] =
    Behaviors.setup[Nothing](context => new IotSupervisor(context))
class IotSupervisor(context: ActorContext[Nothing]) extends AbstractBehavior[Nothing](context):
  context.log.info("Iot App started")
  println("yeah yeah it started")

  override def onMessage(msg: Nothing): Behavior[Nothing] = Behaviors.unhandled

  override def onSignal: PartialFunction[Signal, Behavior[Nothing]] =
    case PostStop =>
      context.log.info("Iot App stopped")
      this


