package iot

import org.apache.pekko.actor.typed.ActorSystem

object IotApp {

  def main(args: Array[String]): Unit = {
    println("hello ?")
    // Create ActorSystem and top level supervisor
    ActorSystem[Nothing](IotSupervisor(), "iot-system")
  }

}