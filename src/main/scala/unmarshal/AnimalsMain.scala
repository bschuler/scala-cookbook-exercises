package unmarshal

import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.stream.Materializer

import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext

object AnimalsMain:

  def main(args: Array[String]): Unit =
    given ExecutionContext = ExecutionContext.global
    val dogString =
      """
        |{"name":"fido",
      "legs":"4",
      "tail":"fluffy"}
        |""".stripMargin
//
//    val fDog: Future[Dog] = Unmarshal[String](dogString).to[Dog]
//    fDog.onComplete {
//      case Success(dog) => println("woof!")
//      case Failure(e)   => println(s"awoo :$e")
//    }
//    println("waiting for the future")
