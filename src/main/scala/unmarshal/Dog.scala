package unmarshal

import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

object Dog extends DefaultJsonProtocol with SprayJsonSupport:
  given RootJsonFormat[Dog] = jsonFormat3(
    Dog.apply
  )

case class Dog(
  name: String,
  tail: String,
  legs: Int
) extends Animal
