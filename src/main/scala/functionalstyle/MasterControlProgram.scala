package functionalstyle

import functionalstyle.MasterControlProgram.{GracefulShutdown, SpawnJob}
import org.apache.pekko
import org.apache.pekko.NotUsed
import pekko.actor.typed.{ActorSystem, Behavior, PostStop, Terminated}
import pekko.actor.typed.scaladsl.Behaviors

object MasterControlProgram:
  sealed trait Command
  final case class SpawnJob(name: String) extends Command
  case object GracefulShutdown            extends Command

  def apply(): Behavior[Command] =
    Behaviors
      .receive[Command] { (context, message) =>
        message match
          case SpawnJob(jobName) =>
            context.log.info("Spawning job {}!", jobName)
            context.spawn(Job(jobName), name = jobName)
            Behaviors.same
          case GracefulShutdown =>
            context.log.info("Initiating graceful shutdown...")
            // Here it can perform graceful stop (possibly asynchronous) and when completed
            // return `Behaviors.stopped` here or after receiving another message.
            Behaviors.stopped
      }
      .receiveSignal { case (context, PostStop) =>
        context.log.info("Master Control Program stopped")
        Behaviors.same
      }

object Job:
  sealed trait Command

  def apply(name: String): Behavior[Command] =
    Behaviors.receiveSignal[Command] { case (context, PostStop) =>
      context.log.info("Worker {} stopped", name)
      Behaviors.same
    }

object MCPMain extends App:
  def apply(): Behavior[NotUsed] =
    Behaviors.setup { context =>
      val mcp = context.spawn(MasterControlProgram(),"MCP")
      mcp ! SpawnJob("cleanTheHouse")
      mcp ! SpawnJob("washTheDishes")
      Thread.sleep(2000)
      mcp ! GracefulShutdown

      Behaviors.receiveSignal:
        case (_, Terminated(_)) =>
          context.log.info("Destroying guardian")
          Behaviors.stopped
    }

  ActorSystem(MCPMain(), "HelloWorld")
