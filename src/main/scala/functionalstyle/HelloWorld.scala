package functionalstyle

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  Terminated
}
import org.apache.pekko.actor.typed.scaladsl.{ActorContext, Behaviors}

object HelloWorld:
  final case class Greet(whom: String, replyTo: ActorRef[Greeted])
  final case class Greeted(whom: String, from: ActorRef[Greet])

  def apply(): Behavior[Greet] = Behaviors.receive { (context, message) =>
    context.log.info("Hello {}!", message.whom)
    message.replyTo ! Greeted(message.whom, context.self)
    Behaviors.same
  }

object HelloWorldBot:

  def apply(max: Int): Behavior[HelloWorld.Greeted] =
    bot(0, max)

  private def bot(
    greetingCounter: Int,
    max: Int
  ): Behavior[HelloWorld.Greeted] =
    Behaviors.receive { (context, message) =>
      val n = greetingCounter + 1
      context.log.info(s"Greeting $n for ${message.whom}")
      if n == max then Behaviors.stopped
      else
        message.from ! HelloWorld.Greet(message.whom, context.self)
        bot(n, max)
    }

object Main2 extends App:
  def apply(): Behavior[NotUsed] =
    Behaviors.setup { context =>
      import HelloWorld.*
      val hw  = context.spawn(HelloWorld(), "hw")
      val hwb = context.spawn(HelloWorldBot(max = 3), "hwb")

      hw ! Greet("riri", hwb)
      hw ! Greet("fifi", hwb)
      hw ! Greet("loulou", hwb)
      hw ! Greet("donald", hwb)
      context.watch(hwb)

      Behaviors.receiveSignal:
        case (_, Terminated(_)) =>
          context.log.info("Destroying actor zero")
          Behaviors.stopped
    }

  ActorSystem(Main2(), "HelloWorld")
