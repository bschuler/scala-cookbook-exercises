package functionalstyle

import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  Props,
  SpawnProtocol
}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.util.Timeout

import scala.concurrent.{ExecutionContext, Future}
import concurrent.duration.DurationInt
import scala.util.{Failure, Success}
import org.apache.pekko.actor.typed.scaladsl.AskPattern.Askable
import org.apache.pekko.actor.typed.scaladsl.AskPattern.schedulerFromActorSystem

object Hal:
  sealed trait Command
  case class OpenThePodBayDoorsPlease(replyTo: ActorRef[Response])
      extends Command
  case class Response(message: String)

  def apply(): Behaviors.Receive[Hal.Command] = Behaviors.receive {
      case (context, OpenThePodBayDoorsPlease(replyTo)) =>
        println(s"This is my Ref, I am Hal : ${context.self}")
        println(s"This is Dave's Ref, I'm answering to Dave : $replyTo")
        replyTo ! Response("I'm sorry, Dave. I'm afraid I can't do that.")
        Behaviors.same
    }

object Dave:

  sealed trait Command
  // this is a part of the protocol that is internal to the actor itself
  private case class AdaptedResponse(message: String) extends Command

  def apply(hal: ActorRef[Hal.Command]): Behavior[Dave.Command] =
    Behaviors.setup[Command] { context =>
      // asking someone requires a timeout, if the timeout hits without response
      // the ask is failed with a TimeoutException
      implicit val timeout: Timeout = 3.seconds

      // Note: The second parameter list takes a function `ActorRef[T] => Message`,
      // as OpenThePodBayDoorsPlease is a case class it has a factory apply method
      // that is what we are passing as the second parameter here it could also be written
      // as `ref => OpenThePodBayDoorsPlease(ref)`
      context.ask(hal, Hal.OpenThePodBayDoorsPlease.apply) {
        case Success(Hal.Response(message)) => AdaptedResponse(message)
        case Failure(_)                     => AdaptedResponse("Request failed")
      }

      // we can also tie in request context into an interaction, it is safe to look at
      // actor internal state from the transformation function, but remember that it may have
      // changed at the time the response arrives and the transformation is done, best is to
      // use immutable state we have closed over like here.
      val requestId = 1
      context.ask(hal, Hal.OpenThePodBayDoorsPlease.apply) {
        case Success(Hal.Response(message)) =>
          AdaptedResponse(s"$requestId: $message")
        case Failure(_) => AdaptedResponse(s"$requestId: Request failed")
      }

      Behaviors.receiveMessage {
        // the adapted message ends up being processed like any other
        // message sent to the actor
        case AdaptedResponse(message) =>
          context.log.info("Got response from hal: {}", message)
          Behaviors.same
      }
    }

object Hodor extends App:
  println("hello world")
  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "dave")

  given ActorSystem[SpawnProtocol.Command] = system
  given ExecutionContext                   = system.executionContext
  given Timeout                            = Timeout(3.seconds)

  val fhal: Future[ActorRef[Hal.Command]] =
    system.ask(
      SpawnProtocol.Spawn(
        behavior = Hal(),
        name = "hal",
        props = Props.empty,
        _
      )
    )
  fhal.map(hal =>
    val fwssup: Future[ActorRef[Dave.Command]] =
      system.ask(
        SpawnProtocol.Spawn(
          behavior = Dave(hal),
          name = "dave",
          props = Props.empty,
          _
        )
      )
  )
