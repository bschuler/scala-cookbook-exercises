package functionalstyle

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import pekko.actor.typed.{ActorRef, ActorSystem, Behavior, Props, SpawnProtocol}
import pekko.util.Timeout

import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.actor.typed.scaladsl.LoggerOps
import concurrent.duration.DurationInt

object HelloWorldMain:
  def apply(): Behavior[SpawnProtocol.Command] =
    Behaviors.setup { context =>
      // Start initial tasks
      // context.spawn(...)
      context.log.info("FUCK THIS SHIT THIS IS NO CONTEXT")

      SpawnProtocol()
    }
object Greeting extends App:
  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(HelloWorldMain(), "hello")
  given ActorSystem[SpawnProtocol.Command] = system
  system.log.info("FUCK THIS ")

  // needed in implicit scope for ask (?)
  import pekko.actor.typed.scaladsl.AskPattern.*
  val ec: ExecutionContext = system.executionContext
  given ExecutionContext = ec
  val timeout: Timeout     = Timeout(3.seconds)
  given Timeout = timeout

  val greeter: Future[ActorRef[HelloWorld.Greet]] =
    system.ask(
      SpawnProtocol.Spawn(
        behavior = HelloWorld(),
        name = "greeter",
        props = Props.empty,
        _
      )
    )

  val greetedBehavior = Behaviors.receive[HelloWorld.Greeted] {
    (context, message) =>
      context.log.info(s"Greeting for ${message.whom} from ${message.from}")
      Behaviors.stopped
  }

  val greetedReplyTo: Future[ActorRef[HelloWorld.Greeted]] =
    system.ask(
      SpawnProtocol.Spawn(greetedBehavior, name = "", props = Props.empty, _)
    )

  for greeterRef <- greeter; replyToRef <- greetedReplyTo do
    greeterRef ! HelloWorld.Greet("Pekko", replyToRef)
    greeterRef ! HelloWorld.Greet("Scala3", replyToRef)
