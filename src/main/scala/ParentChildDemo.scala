import org.apache.pekko
import pekko.actor.*

case class CreateChild(name: String)
case class Name(name: String)

class Child extends Actor {
  var name = "No name"
  override def postStop(): Unit = {
    println(s"D'oh! They killed me ($name): ${self.path}")
  }
  def receive: Receive = {
    case Name(name) => this.name = name
    case _ => println(s"Child $name got message")
  }
}

class Parent extends Actor {
  def receive: Receive = {
    case CreateChild(name) =>
      println(s"Parent about to create Child ($name)")
      val child  = context.actorOf(Props[Child](), name=s"$name")
      child ! Name(name)
    case _ => println(s"Parent got some other message")
  }
}

object ParentChildDemo extends App {
  val actorSystem = ActorSystem("ParentChildTest")
  val parent = actorSystem.actorOf(Props[Parent](), name = "Parent")

  parent ! CreateChild("Jonathan")
  parent ! CreateChild("Jordan")
  Thread.sleep(500)

  println("Sending Jonathan a PoisonPill")
  val jonathan = actorSystem.actorSelection("/user/Parent/Jonathan")
  jonathan ! PoisonPill

  Thread.sleep(5000)
}
