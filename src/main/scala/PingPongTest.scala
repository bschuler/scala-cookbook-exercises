import org.apache.pekko
import pekko.actor.*

case object PingMessage
case object PongMessage
case object StartMessage
case object StopMessage

class Ping(pong: ActorRef) extends Actor:
  private var count = 0
  private def incrementAndPrint(): Unit =
    count += 1; println("ping")
  def receive: Receive =
    case StartMessage =>
      incrementAndPrint()
      pong ! PingMessage
    case PongMessage =>
      incrementAndPrint()
      if count > 99 then
        sender() ! StopMessage
        println("ping stopped")
        context.stop(self)
      else sender() ! PingMessage
    case _ => println("ping got something unexpected")

class Pong extends Actor:
  def receive: Receive =
    case PingMessage =>
      println(" pong")
      sender() ! PongMessage
    case StopMessage =>
      println("pong stopped")
      context.stop(self)
    case _ => println("Pong got something unexpected.")

object PingPongTest extends App:
  val system = ActorSystem("PingPongSystem")
  val pong   = system.actorOf(Props[Pong](), name = "pong")
  val ping   = system.actorOf(Props(new Ping(pong)), name = "ping")
  ping ! StartMessage
  // system.terminate()
