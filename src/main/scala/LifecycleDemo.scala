import org.apache.pekko
import pekko.actor.*
import actortests.Kenny
import actortests.ForceRestart

object LifecycleDemo extends App {
  val system = ActorSystem("LifecycleDemo")
  val kenny = system.actorOf(Props[Kenny](), name="Kenny")
  println("sending kenny a simple String message")
  kenny ! "hello"
  Thread.sleep(2000)

  println("make kenny restart")
  kenny ! ForceRestart
  Thread.sleep(2000)

  println("stopping kenny")
  system.stop(kenny)
  Thread.sleep(2000)

  println("shutdown server")
  system.terminate()
}
