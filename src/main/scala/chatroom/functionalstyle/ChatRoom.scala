package chatroom.functionalstyle

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.DeathPactException
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  PostStop,
  PreRestart,
  Signal,
  SupervisorStrategy,
  Terminated
}

import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object ChatRoom:
  sealed trait RoomCommand
  final case class GetSession(
    screenName: String,
    replyTo: ActorRef[SessionEvent]
  ) extends RoomCommand
  private final case class PublishSessionMessage(
    screenName: String,
    message: String
  ) extends RoomCommand

  sealed trait SessionEvent
  final case class SessionGranted(handle: ActorRef[PostMessage])
      extends SessionEvent
  final case class SessionDenied(reason: String) extends SessionEvent
  final case class MessagePosted(screenName: String, message: String)
      extends SessionEvent
  final case class WriteMessage(message: String) extends SessionEvent
  case object PoisonPill                         extends SessionEvent

  sealed trait SessionCommand
  final case class PostMessage(message: String) extends SessionCommand
  private final case class NotifyClient(message: MessagePosted)
      extends SessionCommand

  def apply(): Behavior[RoomCommand] =
    chatRoom(List.empty)
  private def chatRoom(
    sessions: List[ActorRef[SessionCommand]]
  ): Behavior[RoomCommand] =
    Behaviors
      .receiveSignal { (context, signal) =>
        signal match
          case PostStop =>
            context.log.info(s"Been killed by $signal")
            Behaviors.same
      }
    Behaviors
      .receive { (context, message) =>
        message match
          case GetSession(screenName, client) =>
            // create a child actor for further interaction with the client
            val ses = context.spawn(
              session(context.self, screenName, client),
              name = URLEncoder.encode(screenName, StandardCharsets.UTF_8.name)
            )
            client ! SessionGranted(ses)
            chatRoom(ses :: sessions)
          case PublishSessionMessage(screenName, message) =>
            val notification = NotifyClient(MessagePosted(screenName, message))
            sessions.foreach(_ ! notification)
            Behaviors.same
      }

  private def session(
    room: ActorRef[PublishSessionMessage],
    screenName: String,
    client: ActorRef[SessionEvent]
  ): Behavior[SessionCommand] =
    Behaviors.receiveMessage {
      case PostMessage(message) =>
        // from client, publish to others via the room
        room ! PublishSessionMessage(screenName, message)
        Behaviors.same
      case NotifyClient(message) =>
        // published from the room
        client ! message
        Behaviors.same
    }
object Gabbler:
  import ChatRoom.*

  private def gabblerBasicBehavior(
    context: ActorContext[SessionEvent],
    message: SessionEvent
  ): Behavior[SessionEvent] =
    message match
      case SessionGranted(handle) =>
        handle ! PostMessage("Hello World!")
        Behaviors.same
      case MessagePosted(screenName, message) =>
        println(
          s"message has been posted by $screenName: $message"
        )
        Behaviors.stopped

  def apply(): Behavior[SessionEvent] =
    Behaviors
      .receive[SessionEvent] { (context, message) =>
        gabblerBasicBehavior(context, message)
      }
      .receiveSignal { case (context, PostStop) =>
        context.log.info("I've been stopped")
        Behaviors.same
      }

object User:
  import ChatRoom.*
  def apply(): Behavior[SessionEvent] =
    var handler: Option[Any] = None
    Behaviors.setup { context =>
      Behaviors.receiveSignal { case (context, Terminated(_)) =>
        println("Noooooooooooooooo")
        context.log.info("Master Control Program stopped")
        Behaviors.same
      }
      Behaviors.receiveMessage:
        case SessionGranted(handle) =>
          handle ! PostMessage("Salut la compagnie")
          handler = Some(handle)
          Behaviors.same
        case MessagePosted(screenName, message) =>
          context.log.info(s"message has been posted by $screenName: $message")
          Behaviors.same
        case WriteMessage(message) =>
          handler match
            case Some(ref: ActorRef[Any]) => ref ! PostMessage(message)
            case _ =>
              context.log.info(
                s"Not yet in the session, illegal message not sent : $message"
              )
          Behaviors.same

    }

object Main extends App:
  def apply(): Behavior[NotUsed] =
    Behaviors.setup { context =>
      //      val chatRoom = context.spawn(ChatRoom(), "chatroom")
      //
      //      val user1 = context.spawn(User(), name = "user1")
      //      val user2 = context.spawn(User(), name = "user2")
      //
      //      user1 ! ChatRoom.WriteMessage("look Ma I'm hacking")
      //      chatRoom ! ChatRoom.GetSession("alice", user1)
      //      chatRoom ! ChatRoom.GetSession("Bob", user2)
      //
      //      Thread.sleep(2000)
      //      user1 ! ChatRoom.WriteMessage("it works now")
      //
      //      val user3 = context.spawn(User(), "user3")
      //      chatRoom ! ChatRoom.GetSession("charlie", user3)
      //
      //      user3 ! ChatRoom.WriteMessage("glad to be here")
      //      user2 ! ChatRoom.WriteMessage("I'm in")
      //
      //      val gabblerRef = context.spawn(Gabbler(), "gabbler")
      //      context.watch(gabblerRef)
      //      chatRoom ! ChatRoom.GetSession("ol’ Gabbler", gabblerRef)
      //
      //      gabblerRef ! ChatRoom.PoisonPill
      //      Behaviors.same

      //      Behaviors.receiveSignal:
      //        case (_, Terminated(_)) => Behaviors.stopped
      println("Starting...")
      val gabbler  = context.spawn(Gabbler(), "gabbler")
      val chatRoom = context.spawn(ChatRoom(), "chatroom")
      context.watch(gabbler)
      chatRoom ! ChatRoom.GetSession("ol’ Gabbler", gabbler)
      Thread.sleep(2000)
      println("we got here")
      Behaviors.receiveSignal:
        case (_, Terminated(_)) => Behaviors.stopped
    }

  ActorSystem(Main(), "ChatRoomDemo")
