package chatroom.oopstyle

import chatroom.functionalstyle.ChatRoom
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  PostStop,
  Signal,
  Terminated
}

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

object ChatRoom:
  sealed trait RoomCommand
  final case class GetSession(
    screenName: String,
    replyTo: ActorRef[SessionEvent]
  ) extends RoomCommand

  private final case class PublishSessionMessage(
    screenName: String,
    message: String
  ) extends RoomCommand

  sealed trait SessionEvent
  final case class SessionGranted(handle: ActorRef[PostMessage])
      extends SessionEvent
  final case class SessionDenied(reason: String) extends SessionEvent
  final case class MessagePosted(screenName: String, message: String)
      extends SessionEvent

  sealed trait SessionCommand
  final case class PostMessage(message: String) extends SessionCommand
  private final case class NotifyClient(message: MessagePosted)
      extends SessionCommand

  def apply(): Behavior[RoomCommand] =
    Behaviors.setup(context => new ChatRoomBehavior(context))

  class ChatRoomBehavior(context: ActorContext[RoomCommand])
      extends AbstractBehavior[RoomCommand](context):
    private var sessions: List[ActorRef[SessionCommand]] = List.empty

    override def onMessage(message: RoomCommand): Behavior[RoomCommand] =
      message match
        case GetSession(screenName, client) =>
          // create a child actor for further interaction with the client
          val ses = context.spawn(
            SessionBehavior(context.self, screenName, client),
            name = URLEncoder.encode(screenName, StandardCharsets.UTF_8.name)
          )
          client ! SessionGranted(ses)
          sessions = ses :: sessions
          this
        case PublishSessionMessage(screenName, message) =>
          val notification = NotifyClient(MessagePosted(screenName, message))
          sessions.foreach(_ ! notification)
          this

  private object SessionBehavior:
    def apply(
      room: ActorRef[PublishSessionMessage],
      screenName: String,
      client: ActorRef[SessionEvent]
    ): Behavior[SessionCommand] =
      Behaviors.setup(ctx => new SessionBehavior(ctx, room, screenName, client))

  private class SessionBehavior(
    context: ActorContext[SessionCommand],
    room: ActorRef[PublishSessionMessage],
    screenName: String,
    client: ActorRef[SessionEvent]
  ) extends AbstractBehavior[SessionCommand](context):

    override def onMessage(msg: SessionCommand): Behavior[SessionCommand] =
      msg match
        case PostMessage(message) =>
          // from client, publish to others via the room
          room ! PublishSessionMessage(screenName, message)
          Behaviors.same
        case NotifyClient(message) =>
          // published from the room
          client ! message
          Behaviors.same

object Gabbler:
  import ChatRoom.*

  def apply(): Behavior[SessionEvent] =
    Behaviors.setup(ctx => new Gabbler(ctx))

  private class Gabbler(context: ActorContext[SessionEvent]) extends AbstractBehavior[SessionEvent](context):
    override def onMessage(message: SessionEvent): Behavior[SessionEvent] =
      message match
        case SessionDenied(reason) =>
          context.log.info(s"cannot start chat room session: $reason")
          Behaviors.stopped
        case SessionGranted(handle) =>
          handle ! PostMessage("Hello World!")
          Behaviors.same
        case MessagePosted(screenName, message) =>
          context.log.info(s"message has been posted by $screenName: $message")
          Behaviors.stopped

    override def onSignal: PartialFunction[Signal, Behavior[ChatRoom.SessionEvent]] =
      case PostStop =>
        context.log.info(s"I've been stopped")
        this

object Main extends App:
  def apply(): Behavior[NotUsed] =
    Behaviors.setup { context =>

      val gabbler  = context.spawn(Gabbler(), "gabbler")
      val chatRoom = context.spawn(ChatRoom(), "chatroom")
      chatRoom ! ChatRoom.GetSession("ol’ Gabbler", gabbler)
      context.watch(gabbler)
      Behaviors.same
      Behaviors.receiveSignal:
        case (_, Terminated(_)) => Behaviors.stopped

    }
  ActorSystem(Main(), name = "Main")
