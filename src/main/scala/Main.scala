import org.apache.pekko
import pekko.actor.*


class HelloActor(myName: String) extends Actor {
  def receive: Receive = {
    case "hello" => println(s"hello from $myName")
    case _ => println(s"'huh ?' said $myName")
  }
}

class SmolActor extends Actor {
  def receive: Receive = {
    case _ => println(s"you messaged me")
  }
}

object Main extends App {
  val system: ActorSystem = ActorSystem("HelloSystem")
  private val helloActor: ActorRef = system.actorOf(Props(new HelloActor("Fred")), name = "helloactor")
  private val smol: ActorRef = system.actorOf(Props[SmolActor](), name = "Smol")
  private val smol2 :ActorRef = system.actorOf(Props(new SmolActor()), name = "Smol2")
  private val myActor: ActorRef = system.actorOf(Props(new HelloActor( "George")), name = "myActor")
  smol ! "hi"
  smol2 ! "hi from smol 2"
  helloActor ! "hello"
  helloActor ! "buenos dias"
  myActor ! "buenos dias from myActor"
  system.terminate()
}