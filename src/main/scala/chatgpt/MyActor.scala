package chatgpt

import org.apache.pekko.actor.typed.Behavior
import org.apache.pekko.actor.typed.scaladsl.{ActorContext, Behaviors}
import org.apache.pekko.actor.typed.Signal
import org.apache.pekko.actor.typed.PreRestart

object MyActor:
  sealed trait Command
  case object SomeMessage     extends Command
  case object NihilistMessage extends Command

  def apply(): Behavior[MyActor.Command] =
    Behaviors.setup(context => new MyActor(context).myBehavioralFunction)

class MyActor(context: ActorContext[MyActor.Command]):
  import MyActor.*
  val myBehavioralFunction: Behavior[MyActor.Command] = Behaviors
    .receiveMessagePartial[Command] {
      case SomeMessage =>
        // someHandling logic here
        Behaviors.same
      case NihilistMessage => someOtherBehaviorFunction
    }
    .receiveSignal { case (_, PreRestart) =>
      // someHandling logic here
      Behaviors.same
    }

  val someOtherBehaviorFunction: Behavior[MyActor.Command] = Behaviors.ignore



object WsHandler:
  sealed trait Command
  case object Msg     extends Command
  case object Fail    extends Command
  case object Connect extends Command

  def apply(): Behavior[WsHandler.Command] =
    Behaviors.setup(context => new WsHandler(context).init)

class WsHandler(context: ActorContext[WsHandler.Command]):
  import WsHandler.*
  println("supervised actor started")

  val init: Behavior[WsHandler.Command] = Behaviors
    .receiveMessagePartial[Command] {
      case Fail =>
        println("supervised actor fails now")
        throw new Exception("I failed!")
      case Msg =>
        println("I received a message")
        Behaviors.same
      case Connect =>
        println("fucking finally")
        connect
    }
    .receiveSignal { case (_, PreRestart) =>
      // someHandling logic here
      Behaviors.same
    }

  val connect: Behavior[WsHandler.Command] = Behaviors.ignore
//    Behaviors
//    .receiveMessagePartial {
//      case Fail =>
//        println("failure under connection")
//        throw new Exception("Connection breaks")
//      case Msg =>
//        println("received msg under connection")
//        Behaviors.same
//    }
//    .receiveSignal { case (_, PreRestart) =>
//      // someHandling logic here
//      Behaviors.same
//    }