import org.apache.pekko
import scala.util.{Failure, Random, Success}
import pekko.actor.*
import pekko.pattern.ask
import pekko.util.Timeout

import scala.concurrent.*
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import concurrent.ExecutionContext.Implicits.global

case object AskNameMessage

class AskNameActor extends Actor {
  def receive: Receive = {
    case AskNameMessage => sender() ! "Fred"
    case _ => println("something else")
  }
}
object AskTest extends App{
  val actorSystem = ActorSystem("AskTest")
  val myActor = actorSystem.actorOf(Props(new AskNameActor()), name="test")
  val timeout = Timeout(5 seconds)
  given Timeout = timeout
  (myActor ? AskNameMessage).onComplete{
    case Success(value) => println(s"found $value")
    case Failure(exception) => exception.printStackTrace()
  }
  println("done")

  val future2 = ask(myActor, AskNameMessage).mapTo[String]
  val result2 = Await.result(future2, 1 second)
  println(s"result2 is $result2")

}
