package websocket

import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem, Behavior, Props, SpawnProtocol}
import org.apache.pekko.util.Timeout
import org.apache.pekko.actor.typed.scaladsl.AskPattern.Askable
import org.apache.pekko.actor.typed.scaladsl.AskPattern.schedulerFromActorSystem
import org.apache.pekko.util.Timeout

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

object WsGuardian:
  sealed trait Command

  case object Run extends Command

  def apply(): Behavior[Command] = Behaviors.setup(context =>
    Behaviors.receiveMessagePartial { case Run =>
      val wsSupervisor =
        context.spawn(WsSupervisor(), "wsSupervisor")
      wsSupervisor ! WsSupervisor.CreateWsKennyHandler
      Behaviors.same
    }
  )

object WebsocketMain extends App:
  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "Guardian")
  given ActorSystem[SpawnProtocol.Command] = system
  val ec: ExecutionContext                 = system.executionContext
  given ExecutionContext                   = ec
  given Timeout                            = Timeout(5.seconds)

  val fGuardian: Future[ActorRef[WsGuardian.Command]] = system.ask(
    SpawnProtocol.Spawn(
      behavior = WsGuardian.apply(),
      name = "TheGuardian",
      props = Props.empty,
      _
    )
  )
  fGuardian.onComplete {
    case Success(theGuardian) =>
      theGuardian ! WsGuardian.Run
    case _ => println("fuck my life")
  }
