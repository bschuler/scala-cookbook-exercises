package websocket

import org.apache.pekko.actor.typed.scaladsl.ActorContext
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.actor.typed.scaladsl.TimerScheduler
import org.apache.pekko.actor.typed.{ActorRef, ActorSystem, Behavior, PostStop, PreRestart}
import org.apache.pekko.http.scaladsl.model.ws.BinaryMessage
import org.apache.pekko.http.scaladsl.model.ws.Message
import org.apache.pekko.http.scaladsl.model.ws.TextMessage
import org.apache.pekko.http.scaladsl.model.ws.WebSocketRequest
import org.apache.pekko.http.scaladsl.model.ws.WebSocketUpgradeResponse
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.Http
import org.apache.pekko.stream.scaladsl.Flow
import org.apache.pekko.stream.scaladsl.Keep
import org.apache.pekko.stream.scaladsl.Sink
import org.apache.pekko.stream.scaladsl.Source
import org.apache.pekko.Done

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.Promise
import org.apache.pekko.actor.typed.scaladsl.ActorContext

import scala.concurrent.duration.DurationInt

object WsKennyHandler:
  sealed trait Command
  case class WrappedTextMessage(text: String) extends Command
  case object WrappedNotATextMessage          extends Command
  case object AlarmConnectionBreak extends Command

  case class StartWsConnection(
    timeout: FiniteDuration,
    replyTo: ActorRef[WsKennyHandler.Answer]
  ) extends Command
  case class ConnectionTimedOut(replyTo: ActorRef[WsKennyHandler.Answer])
      extends Command
  case class Connected(replyTo: ActorRef[WsKennyHandler.Answer]) extends Command
  case class RegisterToWs(replyTo: ActorRef[WsKennyHandler.Answer])
      extends Command

  sealed trait Answer
  case object SuccessAnswer extends Answer
  case object FailureAnswer extends Answer

  case object TimerKey

  // Test tools
  case class GetState(replyTo: ActorRef[WsKennyHandler.State]) extends Command
  case class State(state: WsKennyHandlerState)

  def apply(
    parent: ActorRef[WsSupervisor.Command]
  ): Behavior[Command] =
    Behaviors.setup(context =>
      Behaviors.withTimers(timers =>
        new WsKennyHandler(timers, context, parent).init
      )
    )

  enum WsKennyHandlerState:
    case Init, Ready

class WsKennyHandler(
  timers: TimerScheduler[WsKennyHandler.Command],
  context: ActorContext[WsKennyHandler.Command],
  parent: ActorRef[WsSupervisor.Command]
):
  import WsKennyHandler.*

  given ActorSystem[?]   = context.system
  given ExecutionContext = context.executionContext

  val init: Behavior[Command] =
    Behaviors
      .receiveMessagePartial[Command] {
        case GetState(replyTo) =>
          replyTo ! State(WsKennyHandlerState.Init)
          Behaviors.same
        case StartWsConnection(connectionTimeout, replyTo) =>
          context.log.info(s"Starting connection to WS")
          timers.startSingleTimer(
            TimerKey,
            ConnectionTimedOut(replyTo),
            connectionTimeout
          )
          val (upgradeResponse, closed) =
            createWebsocket("kenny")
          closed.onComplete(
            _ => context.self ! AlarmConnectionBreak
          )
          val connected = upgradeResponse.map { upgrade =>
            if upgrade.response.status == StatusCodes.SwitchingProtocols then
              Done
            else
              throw new RuntimeException(
                s"Connection failed: ${upgrade.response.status}"
              )
          }
          connected.map(_ => context.self ! Connected(replyTo))
          Behaviors.same
        case Connected(replyTo) =>
          replyTo ! WsKennyHandler.SuccessAnswer
          timers.cancel(TimerKey)
          ready
        case ConnectionTimedOut(replyTo) =>
          replyTo ! WsKennyHandler.FailureAnswer
          context.log.error("Connection to WS failed: Timeout")
          throw new Exception("ws connection timeout")
        case AlarmConnectionBreak =>
          throw new Exception("Connection has ended")
      }
      .receiveSignal {
        case (_, PreRestart) =>
          context.log.info("Kenny's about to restart")
          parent ! WsSupervisor.ConnectKennyPreRestart
          Behaviors.same
        case (_, PostStop) =>
          context.log.info("Actor Stopped")
          Behaviors.same

      }

  def ready: Behavior[Command] = Behaviors
    .receiveMessagePartial[Command] {
      case GetState(replyTo) =>
        replyTo ! State(WsKennyHandlerState.Ready)
        Behaviors.same
      case WrappedNotATextMessage =>
        context.log.info(
          "Received from WS: something else than a TextMessage. This should not happen."
        )
        Behaviors.same
      case WrappedTextMessage(text) =>
        context.log.info(
          s"Received from WS: message ${text}"
        )
        Behaviors.same
      case AlarmConnectionBreak =>
        throw new Exception("Connection has ended")
    }
    .receiveSignal {
      case (_, PreRestart) =>
        context.log.info("Kenny's about to restart")
        parent ! WsSupervisor.ConnectKenny(2.seconds)
        Behaviors.same

      case(_, PostStop) =>
        context.log.info("Actor Stopped")
        Behaviors.same
    }

  private def createWebsocket(
    appName: String
  ): (
    Future[WebSocketUpgradeResponse],
    Future[Done]
  ) =

    val incoming: Sink[Message, Future[Done]] =
      Sink.foreach {
        case TextMessage.Strict(text) =>
          processTextMessage(text)
        case TextMessage.Streamed(textStream) =>
          textStream.runFold("")(_ + _).map(processTextMessage)
        case bm: BinaryMessage =>
          // Optional: this is only to have a log that this happened
          processBinaryMessage()
          // Mandatory: consume the stream to not block for 5s (pekko.stream.materializer.subscription-timeout)
          // It handles both Strict and Streamed BinaryMessage
          bm.dataStream.runWith(Sink.ignore)
      }

    val outgoing: Source[Message, Promise[Option[Message]]] =
      Source.maybe[Message]

    val flow: Flow[Message, Message, Future[Done]] =
      Flow.fromSinkAndSourceMat(incoming, outgoing)(Keep.left)

    Http().singleWebSocketRequest(
      WebSocketRequest(
        s"ws://10.181.7.21:5039/ari/events?api_key=thariuser:tharipassword&app=$appName"
      ),
      flow
    )

  private def processTextMessage(text: String): Unit =
    context.self ! WsKennyHandler.WrappedTextMessage(text)

  private def processBinaryMessage(): Unit =
    context.self ! WsKennyHandler.WrappedNotATextMessage
