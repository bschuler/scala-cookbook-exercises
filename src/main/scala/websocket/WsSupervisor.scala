package websocket

import org.apache.pekko.actor.typed.{ActorRef, Behavior, SupervisorStrategy}
import org.apache.pekko.actor.typed.scaladsl.{
  ActorContext,
  Behaviors,
  TimerScheduler
}
import WsSupervisor.*

import scala.concurrent.duration.FiniteDuration
import org.apache.pekko.util.Timeout

import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success

object WsSupervisor:
  sealed trait Command

  case object CreateWsKennyHandler                           extends Command
  case class ConnectKenny(connectionTimeout: FiniteDuration) extends Command
  case object ConnectKennyPreRestart extends Command
  case object WsKennyConnected                               extends Command
  case object WsKennyFailure                                 extends Command

  // Test tools
  case class GetState(replyTo: ActorRef[State]) extends Command
  case class State(state: WsSupervisorFsmState)

  case object wsKey

  def apply(): Behavior[Command] =
    Behaviors.setup(context => new WsSupervisor(context).init(None))

  enum WsSupervisorFsmState:
    case Init, Manage

class WsSupervisor(
  context: ActorContext[WsSupervisor.Command]
):
  import WsSupervisor.*

  def init(
    maybeKennyHandler: Option[ActorRef[WsKennyHandler.Command]]
  ): Behavior[WsSupervisor.Command] = Behaviors.withTimers { timers =>
    Behaviors.receiveMessagePartial {
      case GetState(replyTo) =>
        replyTo ! State(WsSupervisorFsmState.Init)
        Behaviors.same
      case CreateWsKennyHandler =>
        val kennyHandler: ActorRef[WsKennyHandler.Command] =
          context.spawn(
            Behaviors
              .supervise(WsKennyHandler(context.self))
              .onFailure(
                SupervisorStrategy.restart
                  .withLimit(maxNrOfRetries = 60, withinTimeRange = 60.seconds)
              ),
            "WsKennyHandler"
          )
        timers.startSingleTimer(wsKey, ConnectKenny(3.seconds), 3.seconds)
        manage(kennyHandler, timers)

    }
  }
  def manage(
    kennyHandler: ActorRef[WsKennyHandler.Command],
    timers: TimerScheduler[Command]
  ): Behavior[Command] =
    Behaviors.receiveMessagePartial {
      case GetState(replyTo) =>
        replyTo ! State(WsSupervisorFsmState.Manage)
        Behaviors.same

      case ConnectKennyPreRestart =>
        timers.startSingleTimer(wsKey,ConnectKenny(3.seconds), 3.seconds)
        Behaviors.same
      case ConnectKenny(connectionTimeout) =>
        given Timeout = connectionTimeout
        context.ask[WsKennyHandler.StartWsConnection, WsKennyHandler.Answer](
          kennyHandler,
          replyTo =>
            WsKennyHandler.StartWsConnection(connectionTimeout, replyTo)
        ) {
          case Success(WsKennyHandler.SuccessAnswer) => WsKennyConnected
          case Success(WsKennyHandler.FailureAnswer) =>
            WsKennyFailure
          case Failure(_) =>
            WsKennyFailure
        }
        Behaviors.same
      case WsKennyConnected =>
        timers.cancel(wsKey)
        context.log.info(s"WS for Kenny: connection successful")
        Behaviors.same
      case WsKennyFailure =>
        context.log.error(s"WS for Kenny: connection failed")
       // timers.startSingleTimer(wsKey,ConnectKenny(3.seconds), 3.seconds)
        Behaviors.same
    }
