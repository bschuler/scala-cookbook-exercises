import org.apache.pekko.actor.typed.{ActorSystem, SpawnProtocol}
import org.apache.pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.apache.pekko.http.scaladsl.unmarshalling.Unmarshal
import org.apache.pekko.stream.{ActorMaterializer, Materializer}
import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

import spray.json.enrichAny

trait myTrait
object BaseAriMessage extends DefaultJsonProtocol with SprayJsonSupport:
  given RootJsonFormat[BaseAriMessage] = jsonFormat2(BaseAriMessage.apply)

case class BaseAriMessage(
  asterisk_id: Option[String],
  `type`: String
) extends myTrait

trait AriMessage:
  val asterisk_id: Option[String]
  val `type`: String

trait TChan:
  val channel: String

trait AriMessageWithChannel(
  asterisk_id: Option[String],
  `type`: String,
  channel: String
) extends AriMessage
    with TChan
//
//object InstanceAriMessage extends DefaultJsonProtocol with SprayJsonSupport:
//  given RootJsonFormat[InstanceAriMessage] =
//    jsonFormat2(
//      InstanceAriMessage.apply
//    )
//
//case class InstanceAriMessage(
//  asterisk_id: Option[String],
//  `type`: String
//) extends AriMessage(asterisk_id, `type`)
//
//object OverrideAriMessage extends DefaultJsonProtocol with SprayJsonSupport:
//  given RootJsonFormat[OverrideAriMessage] =
//    jsonFormat(OverrideAriMessage.apply, "asterisk_id", "type")
//
//case class OverrideAriMessage(
//  asterisk_id: Option[String],
//  `type`: String
//) extends AriMessage(asterisk_id, `type`)
//
//object ExplicitAriMessage extends DefaultJsonProtocol with SprayJsonSupport:
//  given RootJsonFormat[ExplicitAriMessage] =
//    new RootJsonFormat[ExplicitAriMessage]:
//      override def read(json: JsValue): ExplicitAriMessage = json match
//        case _ => ExplicitAriMessage(Some("titi"), "toto")
//      override def write(obj: ExplicitAriMessage): JsValue = obj match
//        case _ => "tata".toJson
//
//case class ExplicitAriMessage(
//  asterisk_id: Option[String],
//  `type`: String
//) extends AriMessage(asterisk_id, `type`)
//
//object ChanAr1 extends DefaultJsonProtocol with SprayJsonSupport:
//  given RootJsonFormat[ChanAr1] =
//    jsonFormat(
//      ChanAr1.apply,
//      "asterisk_id",
//      "type",
//      "channel"
//    )
//
//case class ChanAr1(
//  asterisk_id: Option[String],
//  `type`: String,
//  channel: String
//) extends AriMessage(asterisk_id, `type`)
//    with AriMessageWithChannel(asterisk_id, `type`, channel)

//object ChanAr2 extends DefaultJsonProtocol with SprayJsonSupport:
//  given RootJsonFormat[ChanAr2] =
//    jsonFormat(
//      ChanAr2.apply,
//      "asterisk_id",
//      "type",
//      "channel"
//    )
//
//case class ChanAr2(
//  asterisk_id: Option[String],
//  `type`: String,
//  channel: String
//) extends AriMessageWithChannel(asterisk_id, `type`, channel)

object ChanAr3 extends DefaultJsonProtocol with SprayJsonSupport:
  given RootJsonFormat[ChanAr3] =
    jsonFormat3(ChanAr3.apply)
//      "asterisk_id",
//      "type",
//      "channel"
//    )

case class ChanAr3(
  asterisk_id: Option[String],
  `type`: String,
  channel: String
) extends AriMessage
    with TChan

object ScalaParametrizedTraits extends App:

  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "ConferencerGuardian")
  given ActorSystem[SpawnProtocol.Command] = system
  given ExecutionContext = scala.concurrent.ExecutionContext.global

  val ariJson: String =
    """
      |{    "type": "jsonAsteriskType",
      |    "asterisk_id": "jsonAsteriskId",
      |    "toto":"titi",
      |    "channel":"coco"
      |    }
      |""".stripMargin

  val ariJsonWithoutId: String = """
                                   |{    "type": "jsonAsteriskType",
                                   |    "toto":"titi",
                                   |    "channel":"coco"
                                   |    }
                                   |""".stripMargin
//
//  val baseAriMessage: Future[BaseAriMessage] =
//    Unmarshal[String](ariJson).to[BaseAriMessage]
//  baseAriMessage.onComplete {
//    case Success(ariMessage) =>
//      println(s"unmarshal successful : ${ariMessage.toString}")
//    case Failure(e) => println(s"failure with $e")
//  }
//
//  val explicitAriMessage: Future[ExplicitAriMessage] =
//    Unmarshal[String](ariJson).to[ExplicitAriMessage]
//  explicitAriMessage.onComplete {
//    case Success(ariMessage) =>
//      println(s"unmarshal successful: ${ariMessage.toString}")
//    case Failure(e) => println(s"failure with $e")
//  }
//
//  val overrideAriMessage: Future[OverrideAriMessage] =
//    Unmarshal[String](ariJson).to[OverrideAriMessage]
//  overrideAriMessage.onComplete {
//    case Success(ariMessage) =>
//      println(s"unmarshal successful: ${ariMessage.toString}")
//    case Failure(e) => println(s"failure with $e")
//  }
//
//  val chanAr1: Future[ChanAr1] =
//    Unmarshal[String](ariJson).to[ChanAr1]
//  chanAr1.onComplete {
//    case Success(ariMessage) =>
//      println(s"unmarshal successful: ${ariMessage.toString}")
//      ariMessage match
//        case m1: AriMessageWithChannel =>
//          println(s"Ar1 wins : ${m1.channel}, ${m1.asterisk_id}")
//        case old: AriMessage =>
//          println(s"Ar1 poor match")
//        case _ => println(s"Ar1 looses")
//    case Failure(e) => println(s"failure with $e")
//}

  val chanAr3: Future[Object] =
    Unmarshal[String](ariJsonWithoutId).to[ChanAr3]
  chanAr3.onComplete {
    case Success(ariMessage) =>
      println(s"unmarshal successful: ${ariMessage.toString}")
      ariMessage match
        case m1: AriMessage with TChan =>
          println(m1.getClass)
          println(m1.toString)
          println(s"Ar3 wins : ${m1.channel}, ${m1.asterisk_id}")
//        case old: AriMessage =>
//          println(s"Ar1 poor match")
        case _ => println(s"Ar3 looses")
    case Failure(e) => println(s"failure with $e")
  }

//  val evolvedAriMessage: Future[InstanceAriMessage] =
//    Unmarshal[String](ariJson).to[InstanceAriMessage]
//  evolvedAriMessage.onComplete {
//    case Success(ariMessage) =>
//      println(s"unmarshal successful: ${ariMessage.toString}")
//    case Failure(e) => println(s"failure with $e")
//  }
