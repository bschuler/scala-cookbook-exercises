package actortests

import org.apache.pekko.actor.*

case object ForceRestart

class Kenny extends Actor {
  println("entered the Kenny constructor")
  override def preStart(): Unit = { println("kenny: preStart")}
  override def postStop(): Unit = { println("kenny: postStop")}
  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    println("kenny: preRestart")
    println(s" MESSAGE: ${message.getOrElse("")}")
    println(s" REASON: ${reason.getMessage}")
    super.preRestart(reason, message)
  }
  override def postRestart(reason: Throwable): Unit = {
    println("kenn: postRestart")
    println(s" REASON: ${reason.getMessage}")
    super.postRestart(reason)
  }
  def receive: Receive = {
    case ForceRestart => throw new Exception("Boom!")
    case s:String => println(s"kenny received string $s")
    case _ => println("Kenny received a message")
  }
}