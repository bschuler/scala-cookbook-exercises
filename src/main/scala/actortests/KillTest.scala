package actortests

import org.apache.pekko
import pekko.actor.*

object KillTest extends App{
  val actorSystem = ActorSystem("KillTest")
  val kenny = actorSystem.actorOf(Props[Kenny](),name="kenny")
  kenny ! "hello"
  kenny ! Kill
  kenny ! "still there ?"
  actorSystem.terminate()
}
