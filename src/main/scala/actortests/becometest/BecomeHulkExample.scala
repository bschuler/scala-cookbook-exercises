package actortests.becometest
import org.apache.pekko
import pekko.actor.*

import java.lang.Thread.sleep
import java.util.concurrent.CompletableFuture.AsynchronousCompletionTask
case object ActNormalMessage
case object TryToFindSolution
case object BadGuysMakeMeAngry

class DavidBanner extends Actor {
  import context._

  def angryState: Receive = {
    case ActNormalMessage => println("Back to normal")
      become(normalState)
    case _ => println("ANGRY")

  }
  def normalState :Receive ={
    case TryToFindSolution => println("Looking for solution")
    case BadGuysMakeMeAngry => println("Getting angry")
      become(angryState)
  }
  def receive : Receive = {
    case BadGuysMakeMeAngry => become(angryState)
    case ActNormalMessage => become(normalState)
  }
}
object BecomeHulkExample  extends App {
  val actorSystem = ActorSystem("BecomeHulkExample")
  val davidBanner = actorSystem.actorOf(Props(new DavidBanner() ) , name ="Hulk")

  davidBanner ! TryToFindSolution

  davidBanner ! ActNormalMessage
  davidBanner ! TryToFindSolution
  davidBanner ! BadGuysMakeMeAngry

  sleep(1000)
  davidBanner ! BadGuysMakeMeAngry
  davidBanner ! TryToFindSolution


  davidBanner ! ActNormalMessage
  actorSystem.terminate()




}
