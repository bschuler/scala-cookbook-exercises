package actortests

import org.apache.pekko.actor.Actor

class TestActor extends Actor:
  def receive: Receive = { case _ =>
    println(" a message was received")
  }
  override def postStop(): Unit = println("TestActor: PostStop")
