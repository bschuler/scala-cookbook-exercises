package actortests

import org.apache.pekko
import org.apache.pekko.actor.*

class TestActor2 extends TestActor {
  override def receive: Receive = {
    case s: String => println(s"Message Received : $s")
    case _ => println("actortests.TestActor got a non-string message")
  }
}

object PoisonPillTest extends App{
  val actorSystem = ActorSystem("PoisonPillTest")
  val actor: ActorRef = actorSystem.actorOf(Props[TestActor2](), name = "test")
  actor ! "before PoisonPill"
  actor ! PoisonPill
  actor ! "after PoisonPill"
  Thread.sleep(500)
  val impostorActor: ActorRef = actorSystem.actorOf(Props[TestActor2](), name = "test")
  actor ! "still there ?"
  impostorActor ! "hello"

}
