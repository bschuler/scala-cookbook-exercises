package actortests

import org.apache.pekko.actor.{ActorSystem, Props}

object SystemStopExample extends App:
  val actorSystem = ActorSystem("SystemStopExample")
  val actor       = actorSystem.actorOf(Props[TestActor](), name = "test")
  actor ! "hello"

  actorSystem.stop(actor)
  private val nums = 1 to 50
  for n <- nums do actor ! "still there ?"
