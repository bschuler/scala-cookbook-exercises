package actortests.deathwatch
import org.apache.pekko
import org.apache.pekko.actor.*

case object Explode

class Cartman extends Actor {

  def receive : Receive = {
    case Explode => throw new Exception("Boooooom")
    case _ => println("Cartman received a message ")
  }

  override def preStart(): Unit = {println(" PreStart")}

  override def postStop(): Unit = {println("PostStop")}

  override def preRestart(reason: Throwable, message: Option[Any]): Unit =
    println("preRestart")
    super.preRestart(reason, message)

  override def postRestart(reason: Throwable): Unit =
    println("postRestart")
    super.postRestart(reason)
}
class Mum extends Actor{
  val billy: ActorRef = context.actorOf(Props[Cartman](),name="Cartman")
  context.watch(billy)

  def receive:Receive = {
    case Terminated(billy) => println("OMG, they killed Billy")
    case _ => println("Parent received a message")
  }
}

object RestartTest extends App {

  val actorSystem = ActorSystem("RestartTest")
  val mum: ActorRef = actorSystem.actorOf(Props[Mum](),name="Mum")
  val cartman = actorSystem.actorSelection("/user/Mum/Cartman")

  cartman ! Explode
  Thread.sleep(3000)
  cartman ! "HI"
  println("shutdown")
  actorSystem.terminate()
}
