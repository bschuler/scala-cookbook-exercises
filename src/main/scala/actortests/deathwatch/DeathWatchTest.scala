package actortests.deathwatch
import org.apache.pekko
import pekko.actor.*
import actortests.{ForceRestart, Kenny}

class Parent extends Actor:
  val billy: ActorRef = context.actorOf(Props[Kenny](), name = "Kenny")
  context.watch(billy)

  def receive: Receive = {
    case Terminated(billy) => println("OMG, they killed Billy")
    case _                 => println("Parent received a message")
  }

object DeathWatchTest extends App:
  val actorSystem = ActorSystem("DeathWatchTest")
  val parent      = actorSystem.actorOf(Props[Parent](), name = "Dad")
  val billy       = actorSystem.actorSelection("/user/Dad/Kenny")
  billy ! ForceRestart
  billy ! "Hello"
  Thread.sleep(3000)
  billy ! "Hello"
  println("calling system shutdown")
  actorSystem.terminate()
