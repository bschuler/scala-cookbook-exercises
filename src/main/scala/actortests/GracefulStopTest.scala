package actortests

import org.apache.pekko
import org.apache.pekko.actor.*
import org.apache.pekko.pattern.gracefulStop

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object GracefulStopTest extends App {
  val actorSystem = ActorSystem("GracefulStopTest")
  val testActor = actorSystem.actorOf(Props[TestActor](), name = "test")
  testActor ! "hello"
  try
    val stopped: Future[Boolean] = gracefulStop(target=testActor, timeout=2 seconds)
    val nums = 1 to 50
    for (n <- nums) testActor ! "still there ?"
    Await.result(stopped, 3 seconds)
    println("testActor was stopped")
  catch
    case e: Exception => e.printStackTrace()

}
