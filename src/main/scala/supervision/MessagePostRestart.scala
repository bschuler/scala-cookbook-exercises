package supervision

import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  PostStop,
  PreRestart,
  Props,
  Signal,
  SpawnProtocol,
  SupervisorStrategy
}
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import org.apache.pekko.actor.typed.scaladsl.AskPattern.Askable
import org.apache.pekko.actor.typed.scaladsl.AskPattern.schedulerFromActorSystem
import org.apache.pekko.util.Timeout

import java.lang.Thread.sleep
import concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

object WsSupervisor:
  sealed trait Command
  case object CreateHandler                         extends Command
  case class OrderHandler(order: WsHandler.Command) extends Command
  case object WsConnectionEstablished               extends Command
  case object WsConnectionBroken                    extends Command

  def apply(): Behavior[WsSupervisor.Command] =
    Behaviors.setup(context => new WsSupervisor(context).init)

class WsSupervisor(context: ActorContext[WsSupervisor.Command]):

  import WsSupervisor.*

  val init: Behavior[WsSupervisor.Command] = Behaviors.receiveMessagePartial {
    case CreateHandler =>
      val wsH: ActorRef[WsHandler.Command] = context.spawn(
        Behaviors
          .supervise(WsHandler(context.self))
          .onFailure(
            SupervisorStrategy.restart
              .withLimit(maxNrOfRetries = 2000, withinTimeRange = 5.seconds)
          ),
        name = "wsHandler"
      )
      registered(wsH)
    case OrderHandler(_) =>
      println("no one to send the order to")
      Behaviors.same
  }

  def registered(
    wsH: ActorRef[WsHandler.Command]
  ): Behavior[WsSupervisor.Command] = Behaviors.receiveMessagePartial {
    case CreateHandler =>
      println("handler already created")
      Behaviors.same
    case OrderHandler(order) =>
      wsH ! order
      Behaviors.same
    case WsConnectionEstablished =>
      println("ws connection established")
      Behaviors.same
    case WsConnectionBroken =>
      println("ws connection broken")
      wsH ! WsHandler.Connect
      Behaviors.same
  }

object WsHandler:
  sealed trait Command
  case object Msg     extends Command
  case object Fail    extends Command
  case object Connect extends Command

  def apply(
    wsSupRef: ActorRef[WsSupervisor.Command]
  ): Behavior[WsHandler.Command] =
    Behaviors.setup(context => new WsHandler(context, wsSupRef).init)

class WsHandler(
  context: ActorContext[WsHandler.Command],
  parent: ActorRef[WsSupervisor.Command]
):
  import WsHandler.*
  println("supervised actor started")
  Thread.sleep(1000)

  val init: Behavior[WsHandler.Command] = Behaviors
    .receiveMessagePartial[WsHandler.Command] {
      case Fail =>
        println("supervised actor fails now")
        throw new Exception("I failed!")
      case Msg =>
        println("I received a message")
        Behaviors.same
      case Connect =>
        if math.random < 0.9 then throw new Exception("connection crashed")
        println("I establish my connection")
        connect
    }
    .receiveSignal { case (_, PreRestart) =>
      println("apparently I'm going to restart, I was not yet connected")
      parent ! WsSupervisor.WsConnectionBroken
      Behaviors.same
    }

  val connect: Behavior[WsHandler.Command] = Behaviors.ignore
  Behaviors
    .receiveMessagePartial[WsHandler.Command] {
      case Fail =>
        println("failure under connection")
        throw new Exception("Connection breaks")
      case Msg =>
        println("received msg under connection")
        Behaviors.same
    }
    .receiveSignal { case (_, PreRestart) =>
      println("apparently I'm going to restart, I was connected")
      parent ! WsSupervisor.WsConnectionBroken
      Behaviors.same
    }

object TheGuardian:
  sealed trait Command
  case object Run extends Command

  def apply(): Behavior[Command] = Behaviors.setup(context =>
    Behaviors.receiveMessagePartial { case Run =>
      val wsSupervisor =
        context.spawn(WsSupervisor(), "supervising-actor")
      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Msg)
      wsSupervisor ! WsSupervisor.CreateHandler
      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Connect)
//      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Msg)
//      wsSupervisor ! WsSupervisor.CreateHandler
//      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Fail)
//      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Msg)
//      Thread.sleep(3000)
//      wsSupervisor ! WsSupervisor.OrderHandler(WsHandler.Msg)
      Behaviors.same
    }
  )

object MessagePostRestart extends App:
  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "ConferencerGuardian")
  given ActorSystem[SpawnProtocol.Command] = system
  val ec: ExecutionContext                 = system.executionContext
  given ExecutionContext                   = ec
  given Timeout                            = Timeout(5.seconds)

  val fGuardian: Future[ActorRef[TheGuardian.Command]] = system.ask(
    SpawnProtocol.Spawn(
      behavior = TheGuardian.apply(),
      name = "TheGuardian",
      props = Props.empty,
      _
    )
  )
  fGuardian.onComplete {
    case Success(theGuardian) =>
      theGuardian ! TheGuardian.Run
    case _ => println("fuck my life")
  }
