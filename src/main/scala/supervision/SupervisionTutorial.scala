package supervision

import org.apache.pekko.actor.typed.{
  ActorRef,
  ActorSystem,
  Behavior,
  PostStop,
  PreRestart,
  Props,
  Signal,
  SpawnProtocol,
  SupervisorStrategy
}
import org.apache.pekko.actor.typed.scaladsl.{
  AbstractBehavior,
  ActorContext,
  Behaviors
}
import supervision.GuardianIdol.{Fail, Launch}
import org.apache.pekko.actor.typed.scaladsl.AskPattern.Askable
import org.apache.pekko.actor.typed.scaladsl.AskPattern.schedulerFromActorSystem
import org.apache.pekko.util.Timeout

import java.lang.Thread.sleep
import concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

object SupervisingActor:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new SupervisingActor(context))

class SupervisingActor(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  private val child = context.spawn(
    Behaviors
      .supervise(SupervisedActor())
      .onFailure(
        SupervisorStrategy.restart
          .withLimit(maxNrOfRetries = 2, withinTimeRange = 5.seconds)
      ),
    name = "supervised-actor"
  )

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "failChild" =>
        child ! "fail"
        this

object SupervisedActor:
  def apply(): Behavior[String] =
    Behaviors.setup(context => new SupervisedActor(context))

class SupervisedActor(context: ActorContext[String])
    extends AbstractBehavior[String](context):
  println("supervised actor started")
  throw new Exception("fail on start")

  override def onMessage(msg: String): Behavior[String] =
    msg match
      case "fail" =>
        println("supervised actor fails now")
        // Behaviors.stopped
        throw new Exception("I failed!")

  override def onSignal: PartialFunction[Signal, Behavior[String]] = {
    case PreRestart =>
      println("supervised actor will be restarted")
      this
    case PostStop =>
      println("supervised actor stopped")
      this
  }

object GuardianIdol:
  sealed trait Command
  case object Launch extends Command
  case object Fail   extends Command

  def apply(): Behavior[Command] = Behaviors.setup(context =>
    Behaviors.receiveMessagePartial { case Launch =>
      val supervisingActor =
        context.spawn(SupervisingActor(), "supervising-actor")
      context.self ! Fail
      supervising(supervisingActor)
    }
  )

  def supervising(supervisor: ActorRef[String]): Behavior[Command] =
    Behaviors.receiveMessagePartial { case Fail =>
      supervisor ! "failChild"
      Behaviors.same
    }

object supervisionTutorial extends App:

  val system: ActorSystem[SpawnProtocol.Command] =
    ActorSystem(SpawnProtocol(), "ConferencerGuardian")
  given ActorSystem[SpawnProtocol.Command] = system
  val ec: ExecutionContext                 = system.executionContext
  given ExecutionContext                   = ec
  given Timeout                            = Timeout(5.seconds)

  val fGuardian: Future[ActorRef[GuardianIdol.Command]] = system.ask(
    SpawnProtocol.Spawn(
      behavior = GuardianIdol.apply(),
      name = "GuardianIdol",
      props = Props.empty,
      _
    )
  )
  fGuardian.onComplete {
    case Success(guardianIdol) =>
      guardianIdol ! Launch
      guardianIdol ! Fail
      Thread.sleep(6000)
      guardianIdol ! Fail
      guardianIdol ! Fail
      guardianIdol ! Fail
    case _ => println("fuck my life")
  }
