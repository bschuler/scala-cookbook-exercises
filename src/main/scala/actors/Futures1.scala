package actors

import java.lang.Thread.sleep
import scala.concurrent.Future
import scala.util.{Failure, Random, Success}
import scala.concurrent.duration.*
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.*
import scala.language.postfixOps

object Futures1 extends App:

  private val f = Future {
    sleep(Random().nextInt(500))
    1 + 1
  }

  f.onComplete {
    case Success(value) => println(s" I received $value ")
    case Failure(e)     => e.printStackTrace()
  }
  println("A"); sleep(100)
  println("B"); sleep(100)
  println("C"); sleep(100)
  println("D"); sleep(100)
  println("E"); sleep(100)

  sleep(1000)
