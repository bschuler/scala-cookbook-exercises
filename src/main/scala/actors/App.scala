package actors

import org.apache.pekko.actor.typed.{ActorSystem, Behavior, ChildFailed, Terminated, scaladsl}
import org.apache.pekko.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration.DurationInt

object App:
  def main(args: Array[String]): Unit =
    ActorSystem.create(createParent, "supervision")

  private def createParent = Behaviors.setup((ctx) =>
    val tmp = ctx.spawn(temp, "temp")
    tmp ! "hello"
    tmp ! "child"
    tmp ! "too late"
    Behaviors.empty
  )


  private def temp: Behavior[String] = Behaviors.setup {
    context =>
      println("coucou")
      Behaviors.receiveMessage:
        case "child" =>
          val child = context.spawn(createChild, "child")
          context.watch(child)
          println("adsf")
          Behaviors.receiveSignal:
            case (context, ChildFailed(_)) =>
              context.log.error(s"Children failed")
              Behaviors.same
        case m =>
          println(s"j'ai eu $m")
          Behaviors.same
  }
  private def createChild = Behaviors.withTimers((timers) =>
    timers.startSingleTimer("illegal", "illegal", 1000.milliseconds)
    Behaviors.receive((ctx, msg) =>
      ctx.log.info("Child received msg: {}", msg)
      throw new IllegalStateException("illegal message")
    )
  )
