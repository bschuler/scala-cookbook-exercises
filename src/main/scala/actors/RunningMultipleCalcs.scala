package actors
import java.lang.Thread.sleep
import scala.util.{Failure, Random, Success}
import scala.concurrent.*
import scala.concurrent.ExecutionContext.Implicits.global

object RunningMultipleCalcs extends App:

  println("start ")
  private def runAlgorithm(i: Int): Future[Int] = Future {
    sleep(Random.nextInt(500))
    // sleep(2)
    i + 1
  }

  val result = for
    r1 <- runAlgorithm(10)
    r2 <- runAlgorithm(6)
    r3 <- runAlgorithm(4)
  yield r1 + r2 + r3

  result.onComplete {
    case Success(total) => println(s"total = $total")
    case Failure(e)     => e.printStackTrace()
  }
  println("A");
  sleep(200)
  println("B");
  sleep(200)
  println("C");
  sleep(200)
  println("D");
  sleep(200)
  println("E");
  sleep(200)
