//package actorfsm
//
//import org.apache.pekko.NotUsed
//import org.apache.pekko.actor.typed.ActorRef
//import org.apache.pekko.actor.typed.ActorSystem
//import org.apache.pekko.actor.typed.Behavior
//import org.apache.pekko.actor.typed.scaladsl.ActorContext
//import org.apache.pekko.actor.typed.scaladsl.Behaviors
//import sample.Chopstick.Busy
//import sample.Chopstick.Answer
//import sample.Chopstick.Command
//import sample.Chopstick.Put
//import sample.Chopstick.Take
//import sample.Chopstick.Taken
//import sample.Hakker.Command
//
//import scala.concurrent.duration.*
//
//// Apache Pekko adaptation of
//// http://www.dalnefre.com/wp/2010/08/dining-philosophers-in-humus/
//
///*
// * A Chopstick is an actor, it can be taken, and put back
// */
//object Chopstick:
//  sealed trait Command
//  final case class Take(ref: ActorRef[Answer]) extends Command
//  final case class Put(ref: ActorRef[Answer])  extends Command
//
//  sealed trait Answer
//  final case class Taken(chopstick: ActorRef[Chopstick.Command]) extends Answer
//  final case class Busy(chopstick: ActorRef[Chopstick.Command])  extends Answer
//
//  def apply(): Behavior[Chopstick.Command] = available()
//
//  // When a Chopstick is available, it can be taken by a hakker
//  private def available(): Behavior[Chopstick.Command] =
//    Behaviors.receivePartial { case (context, Take(hakker)) =>
//      hakker ! Taken(context.self)
//      takenBy(hakker)
//    }
//
//  // When a Chopstick is taken by a hakker
//  // It will refuse to be taken by other hakkers
//  // But the owning hakker can put it back
//  private def takenBy(hakker: ActorRef[Answer]): Behavior[Chopstick.Command] =
//    Behaviors.receive {
//      case (ctx, Take(otherHakker)) =>
//        otherHakker ! Busy(ctx.self)
//        Behaviors.same
//      case (_, Put(`hakker`)) =>
//        available()
//      case _ =>
//        // here and below it's left to be explicit about partial definition,
//        // but can be omitted when Behaviors.receiveMessagePartial is in use
//        Behaviors.unhandled
//    }
//
///*
// * A hakker is an awesome dude or dudette who either thinks about hacking or has to eat ;-)
// */
//object Hakker:
//  sealed trait Command
//  case object Think                          extends Command
//  case object Eat                            extends Command
//  final case class HandleAnswer(msg: Answer) extends Command
//
//  def apply(
//    name: String,
//    left: ActorRef[Chopstick.Command],
//    right: ActorRef[Chopstick.Command]
//  ): Behavior[Command] =
//    Behaviors.setup { ctx =>
//      new Hakker(ctx, name, left, right).waiting
//    }
//
//class Hakker(
//  ctx: ActorContext[Hakker.Command],
//  name: String,
//  left: ActorRef[Chopstick.Command],
//  right: ActorRef[Chopstick.Command]
//):
//
//  import Hakker.*
//
//  private val adapter = ctx.messageAdapter(HandleAnswer)
//
//  val waiting: Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial { case Think =>
//      ctx.log.info("{} starts to think", name)
//      startThinking(ctx, 5.seconds)
//    }
//
//  // When a hakker is thinking it can become hungry
//  // and try to pick up its chopsticks and eat
//  private val thinking: Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial { case Eat =>
//      left ! Chopstick.Take(adapter)
//      right ! Chopstick.Take(adapter)
//      hungry
//    }
//
//  // When a hakker is hungry it tries to pick up its chopsticks and eat
//  // When it picks one up, it goes into wait for the other
//  // If the hakkers first attempt at grabbing a chopstick fails,
//  // it starts to wait for the response of the other grab
//  private lazy val hungry: Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial {
//      case HandleAnswer(Taken(`left`)) =>
//        waitForOtherChopstick(chopstickToWaitFor = right, takenChopstick = left)
//
//      case HandleAnswer(Taken(`right`)) =>
//        waitForOtherChopstick(chopstickToWaitFor = left, takenChopstick = right)
//
//      case HandleAnswer(Busy(_)) =>
//        firstChopstickDenied
//    }
//
//  // When a hakker is waiting for the last chopstick it can either obtain it
//  // and start eating, or the other chopstick was busy, and the hakker goes
//  // back to think about how he should obtain his chopsticks :-)
//  private def waitForOtherChopstick(
//    chopstickToWaitFor: ActorRef[Chopstick.Command],
//    takenChopstick: ActorRef[Chopstick.Command]
//  ): Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial {
//      case HandleAnswer(Taken(`chopstickToWaitFor`)) =>
//        ctx.log.info(
//          "{} has picked up {} and {} and starts to eat",
//          name,
//          left.path.name,
//          right.path.name
//        )
//        startEating(ctx, 5.seconds)
//
//      case HandleAnswer(Busy(`chopstickToWaitFor`)) =>
//        takenChopstick ! Put(adapter)
//        startThinking(ctx, 10.milliseconds)
//    }
//
//  // When a hakker is eating, he can decide to start to think,
//  // then he puts down his chopsticks and starts to think
//  private lazy val eating: Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial { case Think =>
//      ctx.log.info("{} puts down his chopsticks and starts to think", name)
//      left ! Put(adapter)
//      right ! Put(adapter)
//      startThinking(ctx, 5.seconds)
//    }
//
//  // When the results of the other grab comes back,
//  // he needs to put it back if he got the other one.
//  // Then go back and think and try to grab the chopsticks again
//  private lazy val firstChopstickDenied: Behavior[Hakker.Command] =
//    Behaviors.receiveMessagePartial {
//      case HandleAnswer(Taken(chopstick)) =>
//        chopstick ! Put(adapter)
//        startThinking(ctx, 10.milliseconds)
//      case HandleAnswer(Busy(_)) =>
//        startThinking(ctx, 10.milliseconds)
//    }
//
//  private def startThinking(
//    ctx: ActorContext[Hakker.Command],
//    duration: FiniteDuration
//  ) =
//    Behaviors.withTimers[Hakker.Command] { timers =>
//      timers.startSingleTimer(Eat, Eat, duration)
//      thinking
//    }
//
//  private def startEating(
//    ctx: ActorContext[Hakker.Command],
//    duration: FiniteDuration
//  ) =
//    Behaviors.withTimers[Hakker.Command] { timers =>
//      timers.startSingleTimer(Think, Think, duration)
//      eating
//    }
//
//object DiningHakkers:
//
//  def apply(): Behavior[NotUsed] = Behaviors.setup { context =>
//    // Create 5 chopsticks
//    val chopsticks =
//      for (i <- 1 to 5)
//        yield context.spawn(Chopstick(), "Chopstick" + i)
//
//    // Create 5 awesome hakkers and assign them their left and right chopstick
//    val hakkers =
//      for (name, i) <- List(
//          "Ghosh",
//          "Boner",
//          "Klang",
//          "Krasser",
//          "Manie"
//        ).zipWithIndex
//      yield context.spawn(
//        Hakker(name, chopsticks(i), chopsticks((i + 1) % 5)),
//        name
//      )
//
//    // Signal all hakkers that they should start thinking, and watch the show
//    hakkers.foreach(_ ! Hakker.Think)
//
//    Behaviors.empty
//  }
//
//  def main(args: Array[String]): Unit =
//    ActorSystem(DiningHakkers(), "DiningHakkers")
