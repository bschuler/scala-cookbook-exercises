package iot

import actortests.PoisonPillTest
import iot.Device.{PoisonPill, RecordTemperature, TemperatureRecorded}
import iot.DeviceManager.{
  DeviceRegistered,
  ReplyDeviceList,
  RequestDeviceList,
  RequestTrackDevice
}
import org.apache.pekko
import org.apache.pekko.actor.typed.{internal, ActorRef}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike

import scala.collection
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class DeviceManagerSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike:
  import DeviceManager.*

  "be able to register a device group" in {
    val probe     = createTestProbe[DeviceRegistered]()
    val registred = spawn(DeviceManager())

    registred ! RequestTrackDevice("group", "device1", probe.ref)
    probe.receiveMessage()

  }

  "not able to register same device group twice" in {
    val deviceManager         = spawn(DeviceManager())
    val probeDeviceRegistered = createTestProbe[DeviceRegistered]()

    deviceManager ! RequestTrackDevice(
      "group",
      "device1",
      probeDeviceRegistered.ref
    )
    val r1 = probeDeviceRegistered.receiveMessage()
    deviceManager ! RequestTrackDevice(
      "group",
      "device1",
      probeDeviceRegistered.ref
    )
    val r2 = probeDeviceRegistered.receiveMessage()

    r1 should ===(r2)
  }

  "retrieve device Ids from groups " in {
    val probeDeviceRegistered = createTestProbe[DeviceRegistered]()
    val deviceManager         = spawn(DeviceManager())
    val probeReply            = createTestProbe[ReplyDeviceList]()
    deviceManager ! RequestTrackDevice(
      "group1",
      "deviceA",
      probeDeviceRegistered.ref
    )
    val drA = probeDeviceRegistered.receiveMessage()

    deviceManager ! RequestDeviceList(1, "group1", probeReply.ref)
    probeReply.expectMessage(ReplyDeviceList(1, Set("deviceA")))

    deviceManager ! RequestTrackDevice(
      "group2",
      "deviceB",
      probeDeviceRegistered.ref
    )
    val drB = probeDeviceRegistered.receiveMessage()

    deviceManager ! RequestDeviceList(2, "group2", probeReply.ref)
    probeReply.expectMessage(ReplyDeviceList(2, Set("deviceB")))
  }
