package iot

import org.apache.pekko
import pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike

class DeviceSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike:
  import Device.*

  "Device actor" must {

    "reply with empty reading if no temperature is known" in {
      val probe       = createTestProbe[RespondTemperature]()
      val deviceActor = spawn(Device("group", "device"))

      deviceActor ! Device.ReadTemperature(requestId = 42, probe.ref)
      val response = probe.receiveMessage()
      response.requestId should ===(42)
      response.value should ===(None)
    }

    "answer with latest temp" in {
      val recordProbe = createTestProbe[TemperatureRecorded]()
      val readProbe   = createTestProbe[RespondTemperature]()
      val deviceActor = spawn(Device("group", "device"))

      deviceActor ! Device.RecordTemperature(1, 24.0, recordProbe.ref)
      recordProbe.expectMessage(Device.TemperatureRecorded(1))

      deviceActor ! Device.ReadTemperature(2, readProbe.ref)
      val response1 = readProbe.receiveMessage()
      response1.requestId should ===(2)
      response1.value should ===(Some(24.0))

      deviceActor ! Device.RecordTemperature(3, 12.4, recordProbe.ref)
      recordProbe.expectMessage(Device.TemperatureRecorded(3))

      deviceActor ! Device.ReadTemperature(4, readProbe.ref)
      val response2 = readProbe.receiveMessage()
      response2.requestId should ===(
        4
      )
      response2.value should ===(Some(12.4))

    }

  }
