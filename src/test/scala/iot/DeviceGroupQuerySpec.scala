package iot
import actortests.PoisonPillTest
import iot.Device.{PoisonPill, RecordTemperature, TemperatureRecorded}
import iot.DeviceManager.{DeviceNotAvailable, DeviceRegistered, DeviceTimedOut, ReplyDeviceList, RequestAllTemperatures, RequestDeviceList, RequestTrackDevice, RespondAllTemperatures, Temperature, TemperatureNotAvailable}
import org.apache.pekko
import org.apache.pekko.actor.testkit.typed.scaladsl.TestProbe
import org.apache.pekko.actor.typed.internal
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.scalatest.wordspec.AnyWordSpecLike

import scala.collection
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
class DeviceGroupQuerySpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike:
  import DeviceGroupQuery.*

  "return temperature value for working devices using device actor" in {
    val requester = TestProbe[RespondAllTemperatures]()

    val device1 = spawn(Device("group", "device1"))
    val device2 = spawn(Device("group", "device2"))

    device1 ! RecordTemperature(
      1,
      1.0,
      createTestProbe[TemperatureRecorded]().ref
    )
    device2 ! RecordTemperature(
      1,
      2.0,
      createTestProbe[TemperatureRecorded]().ref
    )

    val queryActor =
      spawn(
        DeviceGroupQuery(
          Map("device1" -> device1.ref, "device2" -> device2.ref),
          requestId = 1,
          requester = requester.ref,
          timeout = 3.seconds
        )
      )

    requester.expectMessage(
      RespondAllTemperatures(
        requestId = 1,
        temperatures =
          Map("device1" -> Temperature(1.0), "device2" -> Temperature(2.0))
      )
    )
  }

  "return temperature value for working devices using device probes" in {
    val requester = createTestProbe[RespondAllTemperatures]()

    val device1 = createTestProbe[Device.Command]()
    val device2 = createTestProbe[Device.Command]()

    val deviceIdToActor =
      Map("device1" -> device1.ref, "device2" -> device2.ref)

    val queryActor =
      spawn(
        DeviceGroupQuery(
          deviceIdToActor,
          requestId = 1,
          requester = requester.ref,
          timeout = 3.seconds
        )
      )

    device1.expectMessageType[Device.ReadTemperature]
    device2.expectMessageType[Device.ReadTemperature]

    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device1", Some(1.0))
    )
    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device2", Some(2.0))
    )

    requester.expectMessage(
      RespondAllTemperatures(
        requestId = 1,
        temperatures =
          Map("device1" -> Temperature(1.0), "device2" -> Temperature(2.0))
      )
    )
  }

  "return DeviceNotAvailable if device stops before answering" in {
    val requester = createTestProbe[RespondAllTemperatures]()

    val device1 = createTestProbe[Device.Command]()
    val device2 = createTestProbe[Device.Command]()

    val queryActor =
      spawn(
        DeviceGroupQuery(
          Map("device1" -> device1.ref, "device2" -> device2.ref),
          requestId = 1,
          requester = requester.ref,
          timeout = 3.seconds
        )
      )

    device1.expectMessageType[Device.ReadTemperature]
    device2.expectMessageType[Device.ReadTemperature]

    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device1", Some(2.0))
    )

    device2.stop()

    requester.expectMessage(
      RespondAllTemperatures(
        requestId = 1,
        temperatures =
          Map("device1" -> Temperature(2.0), "device2" -> DeviceNotAvailable)
      )
    )
  }

  "return TemperatureNotAvailable for devices with no readings" in {
    val requester = createTestProbe[RespondAllTemperatures]()

    val device1 = createTestProbe[Device.Command]()
    val device2 = createTestProbe[Device.Command]()

    val deviceIdToActor =
      Map("device1" -> device1.ref, "device2" -> device2.ref)

    val queryActor =
      spawn(
        DeviceGroupQuery(
          deviceIdToActor,
          requestId = 1,
          requester = requester.ref,
          timeout = 3.seconds
        )
      )

    device1.expectMessageType[Device.ReadTemperature]
    device2.expectMessageType[Device.ReadTemperature]

    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device1", None)
    )
    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device2", Some(2.0))
    )

    requester.expectMessage(
      RespondAllTemperatures(
        requestId = 1,
        temperatures = Map(
          "device1" -> TemperatureNotAvailable,
          "device2" -> Temperature(2.0)
        )
      )
    )
  }

  "return temperature reading even if device stops after answering" in {
    val requester = createTestProbe[RespondAllTemperatures]()

    val device1 = createTestProbe[Device.Command]()
    val device2 = createTestProbe[Device.Command]()

    val deviceIdToActor =
      Map("device1" -> device1.ref, "device2" -> device2.ref)

    val queryActor =
      spawn(
        DeviceGroupQuery(
          deviceIdToActor,
          requestId = 1,
          requester = requester.ref,
          timeout = 3.seconds
        )
      )

    device1.expectMessageType[Device.ReadTemperature]
    device2.expectMessageType[Device.ReadTemperature]

    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device1", Some(1.0))
    )
    queryActor ! WrappedRespondTemperature(
      Device.RespondTemperature(requestId = 0, "device2", Some(2.0))
    )

    device2.stop()

    requester.expectMessage(
      RespondAllTemperatures(
        requestId = 1,
        temperatures =
          Map("device1" -> Temperature(1.0), "device2" -> Temperature(2.0))
      )
    )
  }

