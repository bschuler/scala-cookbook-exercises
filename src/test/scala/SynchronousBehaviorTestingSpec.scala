import org.apache.pekko
import pekko.actor.testkit.typed.CapturedLogEvent
import pekko.actor.testkit.typed.Effect.*
import pekko.actor.testkit.typed.scaladsl.{
  BehaviorTestKit,
  ScalaTestWithActorTestKit,
  TestInbox
}
import pekko.actor.typed.*
import pekko.actor.typed.scaladsl.*
import com.typesafe.config.ConfigFactory
import org.apache.pekko.testkit.{ImplicitSender, TestKit}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.slf4j.event.Level

object Hello:
  sealed trait Command

  case object CreateAnonymousChild extends Command

  case class CreateChild(childName: String) extends Command

  case class SayHelloToChild(childName: String) extends Command

  case object SayHelloToAnonymousChild extends Command

  case class SayHello(who: ActorRef[String]) extends Command

  case class LogAndSayHello(who: ActorRef[String]) extends Command

  val childActor = Behaviors.receiveMessage[String] { _ =>
    Behaviors.same[String]
  }

  def apply(): Behaviors.Receive[Command] = Behaviors.receivePartial {
    case (context, CreateChild(name)) =>
      context.spawn(childActor, name)
      Behaviors.same
    case (context, CreateAnonymousChild) =>
      context.spawnAnonymous(childActor)
      Behaviors.same
    case (context, SayHelloToChild(childName)) =>
      val child: ActorRef[String] = context.spawn(childActor, childName)
      child ! "hello"
      Behaviors.same
    case (context, SayHelloToAnonymousChild) =>
      val child: ActorRef[String] = context.spawnAnonymous(childActor)
      child ! "hello stranger"
      Behaviors.same
    case (_, SayHello(who)) =>
      who ! "hello"
      Behaviors.same
    case (context, LogAndSayHello(who)) =>
      context.log.info("Saying hello to {}", who.path.name)
      who ! "hello"
      Behaviors.same
  }

class SynchronousBehaviorTestingSpec
    extends AnyWordSpecLike
    with BeforeAndAfterAll:

  import Hello.*

  override def beforeAll(): Unit =
    super.beforeAll()

  override def afterAll(): Unit =
    super.afterAll()

  "An Hello actor" must {

    "spawn childs on CreateChild" in {
      val testKit = BehaviorTestKit(Hello())
      testKit.run(Hello.CreateChild("child"))
      testKit.expectEffect(Spawned(childActor, "child"))
    }

    "spawn anonymous children" in {
      val testKit = BehaviorTestKit(Hello())
      testKit.run(Hello.CreateAnonymousChild)
      testKit.expectEffect(SpawnedAnonymous(childActor))
    }

    "send message" in {
      val testKit = BehaviorTestKit(Hello())
      val inbox = TestInbox[String]()
      testKit.run(Hello.SayHello(inbox.ref))
      inbox.expectMessage("hello")
    }

    "send message to child actor" in {
      val testKit = BehaviorTestKit(Hello())
      testKit.run(Hello.SayHelloToChild("child"))
      val childInbox = testKit.childInbox[String]("child")
      childInbox.expectMessage("hello")
    }

    "send message to anonymous child" in {
      val testKit = BehaviorTestKit(Hello())
      testKit.run(Hello.SayHelloToAnonymousChild)
      val child = testKit.expectEffectType[SpawnedAnonymous[String]]

      val childInbox = testKit.childInbox(child.ref)
      childInbox.expectMessage("hello stranger")
    }

  }
