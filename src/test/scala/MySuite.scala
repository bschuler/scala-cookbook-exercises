//https://scalameta.org/munit/docs/getting
import org.apache.pekko
import org.apache.pekko.actor.typed.{ActorRef, Behavior}
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import pekko.actor.testkit.typed.scaladsl.LogCapturing
import pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKitBase
import org.scalatest.wordspec.{AnyWordSpec, AnyWordSpecLike}

object Echo:
  case class Ping(message: String, response: ActorRef[Pong])
  case class Pong(message: String)

  def apply(): Behavior[Ping] = Behaviors.receiveMessage {
    case Ping(m, replyTo) =>
      replyTo ! Pong(m)
      Behaviors.same
  }

trait ExtendTestMultipleTimes
    extends ScalaTestWithActorTestKitBase
    with AnyWordSpecLike:
  "ScalaTestWithActorTestKitBase" must {
    "behave when extended in different ways" in {
      val pinger  = testKit.spawn(Echo(), "ping")
      val probe   = testKit.createTestProbe[Echo.Pong]()
      val message = this.getClass.getSimpleName
      pinger ! Echo.Ping(message, probe.ref)
      val returnedMessage = probe.expectMessage(Echo.Pong(message))
      returnedMessage.message.contains("ExtendTestMultipleTimes") shouldBe false
    }
  }

class TestWithOneImplementation
    extends ScalaTestWithActorTestKit
    with ExtendTestMultipleTimes
class TestWithAnotherImplementation
    extends ScalaTestWithActorTestKit
    with ExtendTestMultipleTimes
