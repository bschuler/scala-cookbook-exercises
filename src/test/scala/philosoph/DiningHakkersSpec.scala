package philosoph

import org.apache.pekko.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import org.apache.pekko.actor.typed.ActorRef
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.testkit
import org.scalatest.wordspec.AnyWordSpecLike
import philosophs.Chopstick.{Busy, Taken}
import philosophs.Hakker.{HandleChopstickAnswer, Think}
import philosophs.{Chopstick, Hakker}

import concurrent.duration.DurationInt

class DiningHakkersSpec extends ScalaTestWithActorTestKit with AnyWordSpecLike:

  "should tell hakker that it is available" in {
    val hakkerProbe = createTestProbe[Chopstick.ChopstickAnswer]()
    val chopStick: ActorRef[Chopstick.ChopstickMessage] =
      testKit.spawn(Chopstick.available())

    chopStick ! Chopstick.Take(hakkerProbe.ref)
    hakkerProbe.expectMessage(Taken(chopStick.ref))
  }

  "should tell hakker that it is not available" in {
    val hakkerProbe = createTestProbe[Chopstick.ChopstickAnswer]()
    val chopStick: ActorRef[Chopstick.ChopstickMessage] =
      testKit.spawn(Chopstick.takenBy(hakkerProbe.ref))

    chopStick ! Chopstick.Take(hakkerProbe.ref)
    hakkerProbe.expectMessage(Busy(chopStick.ref))
  }

  "should stop thinking after some time" in {
    val rightProbe  = createTestProbe[Chopstick.ChopstickMessage]()
    val leftProbe   = createTestProbe[Chopstick.ChopstickMessage]()
    val hakkerProbe = createTestProbe[Hakker.Command]()

    val hakkerStartThinking = spawn(
      Behaviors.monitor(
        hakkerProbe.ref,
        Behaviors.setup(context =>
          new Hakker(context, "toto", leftProbe.ref, rightProbe.ref).startThinking(context, 10.milliseconds)
        )
      )
    )

    hakkerProbe.expectMessage(110.milliseconds, Hakker.Eat)

  }
