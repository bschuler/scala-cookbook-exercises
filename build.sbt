val scala3Version = "3.3.1"
val PekkoVersion  = "1.0.1"

lazy val root = project
  .in(file("."))
  .settings(
    name         := "actor-test",
    version      := "0.1.0-SNAPSHOT",
    scalaVersion := scala3Version,
    libraryDependencies ++= Seq(
      "org.scalameta"    %% "munit"                     % "0.7.29"     % Test,
      "org.apache.pekko" %% "pekko-actor-typed"         % PekkoVersion,
      "org.apache.pekko" %% "pekko-stream"              % PekkoVersion,
      "org.apache.pekko" %% "pekko-stream-typed"        % PekkoVersion,
      "org.apache.pekko" %% "pekko-testkit"             % PekkoVersion % Test,
      "org.apache.pekko" %% "pekko-http"                % "1.0.0",
      "org.apache.pekko" %% "pekko-actor-testkit-typed" % PekkoVersion % Test,
      "org.apache.pekko" %% "pekko-slf4j"               % PekkoVersion,
      "org.scalatest"    %% "scalatest"                 % "3.2.17"     % Test,
      "ch.qos.logback"    % "logback-classic"           % "1.2.12",
      "io.spray"         %% "spray-json"                % "1.3.6",
      "org.apache.pekko" %% "pekko-http-spray-json"     % "1.0.0"
    )
  )
